# 小组课程设计

#### Description
Front and rear end project team practice

#### Software Architecture
Software architecture description

+ Front-end
  + 后台 Vue+Aioxs
  + 前台 Flutter
+ Back-end
  + Springboot
  + Shiro
  + Redis
  + Mysql
  + Mybatis

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
