import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'dart:async';

import 'package:flutter/material.dart';


getlistByMeans() async {
  for (int i = 0; i <= 0; i++) {
    var choice = ['default', 'downloads', 'likes']; //选择呈现方式
    var apiUrl =
        Uri.parse('http://8.136.140.19:9000/file/list/' + choice[i].toString());
    var response = await http.get(
      apiUrl,
      headers: {"Accept": "application/json;charset=UTF-8"},
    );

    // print('Response status: ${response.statusCode}'); //测试数据请求是否成功，成功打印200
    // print('Response body: ${response.body}');

    var listmap = {}; //将请求下来的数据类型转换成MAP类型
    listmap = json.decode(response.body);
    // print(
    //    "___________________________________________________分割线________________________________________________");
    // print(listmap);
    return listmap;
  }
}

//依据搜索获取文件信息列表
// ignore: non_constant_identifier_names
getlistBySearch(String Info) async {
  var info = Info; //用户输入的参数
  var operation = "file";

  var apiUrl = Uri.parse('http://8.136.140.19:9000/file/list/search?info=' +
      info +
      '&operation=' +
      operation);
  var response = await http.get(
    apiUrl,
    headers: {"Accept": "application/json;charset=UTF-8"},
  );


  var tempmap = {}; //将请求下来的数据类型转换成MAP类型
  tempmap = json.decode(response.body);
  // print(tempmap["data"]);
  return tempmap["data"];
}

addlike() async {
  var uuid = "507db4b914f64eb7a9fef5cebf221eb4"; //用户自主输入的参数

  var apiUrl = Uri.parse('http://8.136.140.19:9000/file/like/' + uuid);
  var response = await http.get(apiUrl);

  // print('Response status: ${response.statusCode}'); //测试数据请求是否成功，成功打印200
  // print('Response body: ${response.body}');

  var tempmap = {}; //将请求下来的数据类型转换成MAP类型
  tempmap = json.decode(response.body);
  // print(tempmap);
}

//获取类型名
getType() async {
  var apiUrl = Uri.parse('http://8.136.140.19:9000/type/list');
  var response = await http.get(
    apiUrl,
    headers: {"Accept": "application/json;charset=UTF-8"},
  );
  // print('Response status: ${response.statusCode}'); //测试数据请求是否成功，成功打印200
  // print('Response body: ${response.body}');
  var tempmap = {}; //将请求下来的数据类型转换成MAP类型
  tempmap = json.decode(response.body);
  return tempmap["data"];
}

getlistByName(String type) async {
  var name = type; //用户自主输入的参数

  var apiUrl = Uri.parse(
    'http://8.136.140.19:9000/file/list/type/' + name,
  );
  var response = await http.get(
    apiUrl,
    headers: {"Accept": "application/json;charset=UTF-8"},
  );

  // print('Response status: ${response.statusCode}'); //测试数据请求是否成功，成功打印200
  // print('Response body: ${response.body}');
  var tempmap = {}; //将请求下来的数据类型转换成MAP类型
  tempmap = json.decode(response.body);
  return tempmap["data"];
}

//用户下载接口
download(String uid) {
  var uuid = uid; //用户自主输入的参数

  var apiUrl = Uri.parse('http://8.136.140.19:9000/file/download/' + uuid);
  return apiUrl;
}
