import 'package:flutter/material.dart';
import 'open.dart';
import 'bar.dart';

void main(List<String> args) {
  runApp(Myapp());
}

class Myapp extends StatelessWidget {
  const Myapp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: open(),
      routes: <String, WidgetBuilder>{'/NewPage': (context) => MainBar()},
    );
  }
}
