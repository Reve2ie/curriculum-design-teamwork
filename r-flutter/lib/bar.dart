import 'package:flutter/material.dart';
import 'downloadpage/download.dart';
import 'searchpage/searchpage.dart';

import 'moneydialog.dart';
import 'about.dart';
import 'categorypage.dart';
import 'upload.dart';
import 'homepage_copy.dart';

class MainBar extends StatefulWidget {
  MainBar({Key? key}) : super(key: key);

  @override
  _MainBarState createState() => _MainBarState();
}

class _MainBarState extends State<MainBar> {
  int _currentIndex = 0;

  final List _pageList = [
    HomePage(),
    UploadPage(),
    CategoryPage(),
  ];
  @override
  Widget build(BuildContext context) {
    final drawerHeader = UserAccountsDrawerHeader(
      accountName: const Text(
        '开发团队：',
      ),
      accountEmail: const Text(
        ' 高志林、张智亮、丛胤杰、张博文、王富鹏',
      ),
      currentAccountPicture: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(150),
            image: DecorationImage(
                image: AssetImage('images/gzdx.jpeg'), fit: BoxFit.cover)),
      ),
    );
    final drawerItems = ListView(
      children: [
        drawerHeader,
        ListTile(
          title: Text(
            '关于',
          ),
          leading: const Icon(Icons.person),
          onTap: () {
            showDialog(
                context: context,
                builder: (context) {
                  return ABoutDialog();
                });
          },
        ),
        ListTile(
          title: Text(
            '打赏',
          ),
          leading: const Icon(Icons.attach_money),
          onTap: () {
            showDialog(
                context: context,
                builder: (context) {
                  return MoneyDialog();
                });
          },
        ),
      ],
    );
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          '欢迎使用学习时刻!',
        ),
        actions: [
          IconButton(
              tooltip: '查找',
              icon: const Icon(Icons.search),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) {
                    return SearchPage(); //返回的是需要跳转单页面
                  },
                ));
              }),
          IconButton(
              tooltip: '传输列表',
              icon: const Icon(Icons.backup),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) {
                    return CardListScreen(); //返回的是需要跳转单页面
                  },
                ));
              }),
        ],
      ),
      body: this._pageList[this._currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: this._currentIndex,
        onTap: (int index) {
          setState(() {
            this._currentIndex = index;
          });
        },
        iconSize: 30.0,
        fixedColor: Colors.blue,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: "首页"),
          BottomNavigationBarItem(icon: Icon(Icons.add), label: "上传"),
          BottomNavigationBarItem(icon: Icon(Icons.apps), label: "分类"),
        ],
      ),
      floatingActionButton: Container(
          height: 65,
          width: 65,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40), color: Colors.white),
          child: FloatingActionButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) {
                    return UploadPage();
                  },
                ));
              },
              child: Icon(
                Icons.add,
                size: 40,
              ))),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      drawer: Drawer(
        child: drawerItems,
      ),
    );
  }
}
