import 'package:demo2/List/list.dart';
import 'package:demo2/tools/loading.dart';
import 'package:flutter/material.dart';
import '../tools/get_info.dart';

class SideListViewMenu extends StatefulWidget {
  @override
  _SideListViewMenuState createState() => _SideListViewMenuState();
}

class _SideListViewMenuState extends State<SideListViewMenu> {
  List<dynamic> _menuStr = [];
  Map _ItemList = {};
  var i = 1;
  var error;
  var loading = true;
  int _selectedIdx = 0;
  final _menuHeight = 50.0;
  final _menuWidth = 100.0;

  initMenu() async {
    var list = await getType();
    return list;
  }

  initItemList() async {
    var maplist = {};
    for (int i = 0; i < _menuStr.length; i++) {
      var templist = await getlistByName(_menuStr[i]);
      var temp = [];
      for (int i = 0; i < templist.length; i++) {
        temp.add(templist[i]);
      }
      maplist[_menuStr[i]] = temp;
    }
    return maplist;
  }

  @override
  void initState() {
    super.initState();
    loading = true;
    try {
      initMenu().then((v) {
        _menuStr = v;
        setState(() {});
        initItemList().then((v) {
          loading = false;
          _ItemList = v;
          setState(() {});
        });
      });
    } catch (e) {
      loading = true;
      error = e;
      print('_________-------' + e.toString());
    }
  }

  getlist() {
    if (loading == false) {
      var list = MyDownloadList(
        key: UniqueKey(),
        arguments: _ItemList["${_menuStr[_selectedIdx]}"],
      );
      return list;
    }
  }

  @override
  Widget build(BuildContext context) {
    var list = getlist();
    return loading
        ? Loadingpage()
        : RefreshIndicator(
            child: Row(
              children: <Widget>[
                Expanded(
                    child: Container(
                      width: _menuWidth,
                      child: ListView.builder(
                        itemCount: _menuStr.length,
                        itemBuilder: (context, index) {
                          String str = _menuStr[index];
                          return GestureDetector(
                            onTap: () {
                              setState(() {
                                _selectedIdx = index;
                              });
                            },
                            child: Column(
                              children: <Widget>[
                                Container(
                                  height: _menuHeight,
                                  color: (_selectedIdx == index)
                                      ? Color.fromRGBO(150, 205, 205, 0)
                                      : Color.fromRGBO(150, 205, 205, 0.5),
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        width: 5,
                                        height: _menuHeight,
                                        color: (_selectedIdx == index)
                                            ? Colors.blue
                                            : Color.fromRGBO(0, 0, 0, 0),
                                      ),
                                      Expanded(
                                        child: Center(
                                          child: Text(str),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                    flex: 1),
                Expanded(
                  child: Container(
                    child: Center(
                      child: loading == true ? Container() : list,
                    ),
                  ),
                  flex: 4,
                )
              ],
            ),
            onRefresh: () => _handlerRefresh(),
          );
  }

  Future<Null> _handlerRefresh() async {
    initMenu().then((v) {
      _menuStr = v;
      initItemList().then((v1) {
        _ItemList = v1;
        setState(() {});
      });
    });
  }
}
