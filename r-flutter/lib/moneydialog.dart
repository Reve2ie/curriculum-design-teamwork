import 'package:flutter/material.dart';

class MoneyDialog extends Dialog {
  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Center(
          child: Container(
        height: 300,
        width: 300,
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(10),
              child: Stack(
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: Text('您的支持是我们最大的动力'),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: InkWell(
                      child: Icon(Icons.close),
                      onTap: () {
                        Navigator.pop(context);
                      },
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            Container(
              child: Image.asset(
                'images/money.jpeg',
                fit: BoxFit.cover,
              ),
              width: 200,
              height: 200,
            ),
          ],
        ),
      )),
    );
  }
}
