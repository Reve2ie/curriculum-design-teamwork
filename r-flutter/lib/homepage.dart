import 'package:flutter/material.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scrollbar(
        // radius: Radius.circular(10),
        // thickness: 10,
        child: ListView(
      padding: EdgeInsets.all(8),
      children: [
        Center(
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                color: Color.fromRGBO(242, 242, 242, 1)
                // border: Border.all(width: 1, color: Colors.red),
                ),
            height: 200,
            child: AspectRatio(aspectRatio: 3 / 4, child: SwiperPage()),
          ),
        ),
        for (int index = 1; index < 10; index++)
          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage('images/logo.jpeg'),
            ),
            title: Text("高等数学1-1"),
            subtitle: Text('2020-2021 第一学期'),
            onTap: () {},
          ),
        Divider(),
        Center(
          child: Text("---人家是有底线的---"),
        )
      ],
    ));
  }
}

class SwiperPage extends StatefulWidget {
  SwiperPage({Key? key}) : super(key: key);

  @override
  _SwiperPageState createState() => _SwiperPageState();
}

class _SwiperPageState extends State<SwiperPage> {
  List<Map> imgList = [
    {
      'url':
          'https://tse1-mm.cn.bing.net/th/id/R-C.41af1a75eb7e0f69c94e3ff0f2b9740c?rik=UxUdlIDvo01FlA&riu=http%3a%2f%2fwww.ecsponline.com%2fupload%2fwxy%2f9787030316042.jpg&ehk=%2buuUtWsDKSGJV4y8atGnAfA15Gn%2fDKavxbiNBTqoolI%3d&risl=&pid=ImgRaw&r=0'
    },
    {
      'url':
          'https://tse1-mm.cn.bing.net/th/id/R-C.8d666fa20cb9f67e8e24ac5f565fadd1?rik=R21S3rtoEB8ieg&riu=http%3a%2f%2fimg12.360buyimg.com%2fn1%2fjfs%2ft2353%2f232%2f1127120326%2f167255%2f1eb68ed2%2f5642d94bN46b78649.jpg&ehk=Oe37JvwsXMu4HcUw1h10Eperwigrs6RNWZStbKSuvQQ%3d&risl=&pid=ImgRaw&r=0'
    },
    {
      'url':
          'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fwww.5jjc.net%2Ftu5jJDEwLmFsaWNkbi5jb20vdGZzY29tL2kyL1RCMVhLMmNSViQ2WHAkNiQ2JDZfISEkMw.jpg&refer=http%3A%2F%2Fwww.5jjc.net&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1638154510&t=8de3b6427ef86639590e23e1ddb6af6e'
    },
    {
      'url':
          'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg3.tbcdn.cn%2Ftfscom%2Fi2%2F352798170%2FTB1lF_bbA.HL1JjSZFlXXaiRFXa_%21%210-item_pic.jpg&refer=http%3A%2F%2Fimg3.tbcdn.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1638154645&t=14409ef3a555da3a66a6afaa226e05a9'
    }
  ];
  @override
  Widget build(BuildContext context) {
    return Swiper(
      itemBuilder: (BuildContext context, int index) {
        return Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              image: DecorationImage(
                  image: NetworkImage(imgList[index]['url']),
                  fit: BoxFit.cover)),
        );
      },
      itemCount: imgList.length,
      pagination: SwiperPagination(),
      control: SwiperControl(color: Colors.white),
      loop: true,
      autoplay: true,
    );
  }
}
