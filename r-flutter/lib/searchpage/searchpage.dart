import 'package:demo2/List/search_list.dart';
import 'package:demo2/searchpage/search_services.dart';
import 'package:demo2/tools/loading.dart';
import 'package:flutter/material.dart';
import '../downloadpage/download.dart';
import '../List/list.dart';
import 'package:http/http.dart' as http;
import "../tools/get_info.dart";

// import 'package:flutter_MyBilibili/pages/home/SearchResultPage.dart';
// import 'package:flutter_MyBilibili/services/search_services.dart';
// import 'package:flutter_MyBilibili/tools/LineTools.dart';

class DrawLine {
  static Container GreyLine() {
    return Container(
      height: 1,
      color: Colors.grey[300],
    );
  }
}

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State {
  _picture() {
    var uuid = "507db4b914f64eb7a9fef5cebf221eb4"; //用户自主输入的参数
    // print('http://8.136.140.19:9000/file/pic/' + uuid);
    return 'http://8.136.140.19:9000/file/pic/' + uuid;
  }

  bool initflag = true;
  TextEditingController searchcontroller = TextEditingController();
  var load = true;
  String keyword = "";
  final List _hotSearchList = ['C语言', '大学物理', '高等数学', '大学化学', '机械制图', '离散数学'];
  List _historyList = [];
  List<dynamic> _resultList = [];

  @override
  void initState() {
    getHistory();
    super.initState();
  }

  getResult() async {
    List<dynamic> list = await getlistBySearch(keyword);
    return list;
  }

  getHistory() async {
    _historyList = await SearchServices.getHistoryList();
    // print(_historyList);
    if (this.mounted) {
      setState(() {});
    }
  }

  Future<Null> _handlerRefresh() async {
    getResult().then((v) {
      v = _resultList;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    key:
    UniqueKey();
    var _body = ListView(
      physics: BouncingScrollPhysics(),
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(10),
          alignment: Alignment.centerLeft,
          child: Text("大家都在搜"),
        ),
        DrawLine.GreyLine(),
        Padding(
          padding: EdgeInsets.all(10),
          child: Wrap(
            spacing: 10,
            children: _hotSearchList.map((title) {
              return Container(
                  height: 40,
                  decoration: BoxDecoration(
                    color: Colors.grey[100],
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                  ),
                  child: TextButton(
                    onPressed: () {
                      setState(() {
                        this.initflag = false;
                        keyword = title;
                        load = true;
                      });
                      doSearch();
                    },
                    child: Text(title),
                  ));
            }).toList(),
          ),
        ),
        Container(
          padding: EdgeInsets.all(10),
          alignment: Alignment.centerLeft,
          child: Text("搜索历史"),
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 10, left: 10, right: 10),
          child: Wrap(
            spacing: 10,
            children: _historyList.map((title) {
              return Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                  child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                        color: Colors.grey[100],
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                      ),
                      child: TextButton(
                        onPressed: () {
                          setState(() {
                            this.initflag = false;
                            keyword = title;
                            load = true;
                          });
                          doSearch();
                        },
                        child: Text(title),
                      )));
            }).toList(),
          ),
        ),
        Offstage(
          //控制按钮是否可见
          offstage: _historyList.length > 0 ? false : true,
          child: Container(
            alignment: Alignment.center,
            child: TextButton(
              onPressed: () {
                clearHistory();
              },
              child: Text("清空历史记录"),
            ),
          ),
        ),
      ],
    );
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Container(
            margin: EdgeInsets.all(8),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(20.0)),
            ),
            //搜索输入框
            child: TextField(
              controller: TextEditingController.fromValue(
                TextEditingValue(
                  text: keyword,
                  selection: TextSelection.fromPosition(
                    //保持光标在最后面
                    TextPosition(
                      affinity: TextAffinity.downstream,
                      offset: keyword.length,
                    ),
                  ),
                ),
              ),
              textInputAction: TextInputAction.search, //键盘上显示搜索按钮
              autofocus: true, //自动获取焦点
              style: TextStyle(
                color: Colors.grey,
              ),
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.search),
                hintText: "搜索",
                border: InputBorder.none,
                fillColor: Colors.white,
                isDense: false,
              ),
              onSubmitted: (text) {
                //提交时候回调
                if (text != "") {
                  setState(() {
                    this.initflag = false;
                    keyword = text;
                    load = true;
                  });
                  doSearch();
                }
              },
              onChanged: (text) {
                keyword = text;
              },
            ),
          ),
          actions: <Widget>[
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(top: 10, right: 15, bottom: 10),
              child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Text(
                  "取消",
                  style: TextStyle(fontSize: 18),
                ),
              ),
            ),
          ],
        ),
        body: initflag
            ? _body
            : (load
                ? const Loadingpage()
                : _resultList.length > 0
                    ? RefreshIndicator(
                        child: MySearchList(arguments: _resultList),
                        onRefresh: () => _handlerRefresh(),
                      )
                    : const Center(
                        child: Text("很抱歉,什么都没有搜到"),
                      )));
  }

  doSearch() async {
    try {
      await SearchServices.setHistoryData(keyword);
      await getHistory();
      getResult().then((v) {
        _resultList = v;
        load = false;
        if (this.mounted) {
          setState(() {});
        }
      });
    } catch (e) {
      // print(e);
    }
  }

  clearHistory() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: Text(
          '确认清空历史记录?',
          textAlign: TextAlign.center,
        ),
        actions: <Widget>[
          TextButton(
            child: Text("取消"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          TextButton(
            child: Text("确定"),
            onPressed: () async {
              await SearchServices.removeHistory();
              _historyList.clear();
              if (this.mounted) {
                setState(() {});
              }
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}
