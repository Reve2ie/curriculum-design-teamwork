import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'dart:async';

class open extends StatefulWidget {
  open({Key? key}) : super(key: key);

  @override
  _openState createState() => _openState();
}

class _openState extends State<open> {
  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        child: Image.asset(
          'images/opening.png',
          fit: BoxFit.cover,
        ));
  }

  void initState() {
    super.initState();
    conutDown();
  }

  void conutDown() {
    var _duration = Duration(seconds: 3);
    Future.delayed(_duration, newPage);
  }

  void newPage() {
    Navigator.of(context).pushReplacementNamed('/NewPage');
  }
}
