import 'package:flutter/material.dart';

class ABoutDialog extends Dialog {
  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Center(
        child: Container(
          width: 350,
          height: 350,
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(10),
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.center,
                      child: Text('关于产品'),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: InkWell(
                        child: Icon(Icons.close),
                      ),
                    )
                  ],
                ),
              ),
              Divider(),
              Container(
                margin: EdgeInsets.only(top: 20),
                height: 100,
                child: ClipOval(
                  child: Image.asset('images/logo.jpeg'),
                ),
              ),
              Container(
                  margin: EdgeInsets.only(top: 20),
                  // decoration: BoxDecoration(color: Colors.red),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Version：0.0.1',
                        style: TextStyle(fontSize: 18.0),
                      ),
                      Text(
                        'Development Tool：Flutter 2.5.3',
                        style: TextStyle(fontSize: 18.0),
                      ),
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
