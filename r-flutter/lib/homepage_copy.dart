import 'dart:convert';
import './tools/loading.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';
import 'tools/get_info.dart';
import 'List/card_grid_screen.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Map> Itemlist = [];
  var loading = true;
  var list;

  getlistByMeans() async {
    for (int i = 0; i <= 0; i++) {
      var choice = ['default', 'downloads', 'likes']; //选择呈现方式
      var apiUrl = Uri.parse(
          'http://8.136.140.19:9000/file/list/' + choice[i].toString());
      var response = await http.get(
        apiUrl,
        headers: {"Accept": "application/json;charset=UTF-8"},
      );

      //print('Response status: ${response.statusCode}'); //测试数据请求是否成功，成功打印200
      //print('Response body: ${response.body}');

      var listmap = {}; //将请求下来的数据类型转换成MAP类型
      listmap = await json.decode(response.body);
      var list = listmap["data"];
      loading = false;
      return list;
    }
  }

  InitItemlist() async {
    Itemlist.clear();
    var list = await getlistByMeans();
    for (int i = 0; i < list.length; i++) {
      Itemlist.add(list[i]);
    }
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    InitItemlist();
  }

  Future<Null> _handlerRefresh() async {
    InitItemlist().then(() {
      setState(() {});
    });

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      child: Scrollbar(
          child: ListView(
            children: [
              Container(
                // width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.fromLTRB(25, 20, 20, 20),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    color: Color.fromRGBO(242, 242, 242, 1)),
                height: 200,
                child: AspectRatio(aspectRatio: 16 / 9, child: SwiperPage()),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(25, 0, 20, 10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    color: Color.fromRGBO(242, 242, 242, 1)),
                height: 65,
                child: AspectRatio(aspectRatio: 16 / 9, child: TextSwiper()),
              ),
              Divider(),
              Container(
                child: loading
                    ? const Loadingpage()
                    : CardGridList(arguments: Itemlist),
                height: MediaQuery.of(context).size.height * 0.7,
              )
            ],
          )),
      onRefresh: () => _handlerRefresh(),
    );
  }
}

class SwiperPage extends StatefulWidget {
  SwiperPage({Key? key}) : super(key: key);

  @override
  _SwiperPageState createState() => _SwiperPageState();
}

class _SwiperPageState extends State<SwiperPage> {
  List<Map> imgList = [
    {'url': "images/ad1.jpg"},
    {'url': "images/ad2.jpg"},
    {'url': "images/ad3.jpg"}
  ];
  @override
  Widget build(BuildContext context) {
    return Swiper(
      itemBuilder: (BuildContext context, int index) {
        return Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              image: DecorationImage(
                  image: AssetImage(imgList[index]['url']), fit: BoxFit.fill)),
        );
      },

      itemCount: imgList.length,
      // pagination: SwiperPagination(),
      control: SwiperControl(color: Colors.white),
      loop: true,
      autoplay: true,
    );
  }
}

class TextSwiper extends StatefulWidget {
  TextSwiper({Key? key}) : super(key: key);

  @override
  _TextSwiperState createState() => _TextSwiperState();
}

class _TextSwiperState extends State<TextSwiper> {
  List<dynamic> TextList = [];
  var loading = true;
  InitPaperList() async {
    TextList.clear();
    var apiUrl = Uri.parse('http://8.136.140.19:9000/data/notices');

    var response = await http
        .get(apiUrl, headers: {"Accept": "application/json;charset=UTF-8"});
    // print('Response status: ${response.statusCode}');
    // print('Response body: ${response.body}');
    TextList.addAll(json.decode(response.body)["data"]);
    setState(() {
      loading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    InitPaperList();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? Center(child: Text("加载中......"))
        : Swiper(
      itemHeight: MediaQuery.of(context).size.height,
      autoplay: true,
      loop: true,
      itemBuilder: (BuildContext context, int index) {
        return Center(
            child: Container(
                child: Column(
                  children: [
                    Text(TextList[index]['notice'] + '：'),
                    Center(
                        child: Text(
                          TextList[index]['info'],
                          maxLines: 2,
                          textAlign: TextAlign.center,
                          // overflow: TextOverflow.ellipsis,
                        ))
                  ],
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                )));
      },
      itemCount: TextList.length,
      scrollDirection: Axis.vertical,
    );
  }
}

