import 'dart:io';
import 'package:demo2/List/local_list.dart';
import 'package:demo2/auto_refresh/auto_refresh.dart';
import 'package:demo2/tools/simple_storage.dart';
import 'package:demo2/downloadpage/downloadserver.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import '../List/list.dart';
import '../List/card_grid_screen.dart';

import 'dart:convert';

import 'package:demo2/tools/simple_storage.dart';

class LoaclServices {
  //插入新的记录
  static setHistoryData(String key, String value) async {
    try {
      await Storage.setString(key, value);
    } catch (e) {
      print("1:" + e.toString());
    }
  }

  //获取记录列表
  static getDonwloadList() async {
    try {
      var set;
      await Storage.getAll().then((value) {
        value.remove("hotSearchList");
        set = value;
      });
      var list = [];
      var listmap = {};
      list.addAll(set);
      var Downloadlist = [];
      for (int i = 0; i < list.length; i++) {
        await Storage.getString(list[i]).then((value) {
          listmap = json.decode(value);
          Downloadlist.add({list[i]: listmap});
        });
      }
      return Downloadlist;
    } catch (e) {
      print("2:" + e.toString());
      return [];
    }
  }

  //删除所有记录
  static removeDonwload(String key) async {
    await Storage.remove(key);
  }
}

class CardListScreen extends StatefulWidget {
  CardListScreen({Key? key}) : super(key: key);

  @override
  _CardListScreenState createState() => _CardListScreenState();
}

class _CardListScreenState extends State<CardListScreen> {
  var list = [];
  @override
  void initState() {
    super.initState();
    try {
      LoaclServices.getDonwloadList().then((v) {
        list = v;
        setState(() {});
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return AutoRefresh(
      duration: const Duration(days: 1),
      child: Scaffold(
        appBar: AppBar(
          title: const Text("下载列表",
              style: TextStyle(fontSize: 20), textAlign: TextAlign.justify),
        ),
        body: SafeArea(
          child: Column(children: [
            const MyDownLoadPath(),
            Expanded(
                child: list.isEmpty
                    ? Center(
                        child: Text("这里什么都没有哦"),
                      )
                    : MyLoaclList(
                        arguments: list,
                      ))
          ]),
        ),
      ),
    );
  }
}

// class AppBarDemoPage extends StatelessWidget {
//   const AppBarDemoPage({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return DefaultTabController(
//       length: 2,
//       child: Scaffold(
//         appBar: AppBar(
//           title: Text("AppBarDemoPage"),
//           backgroundColor: Colors.white,
//           centerTitle: true,
//           bottom: const TabBar(
//             indicatorColor: Colors.blue,
//             labelColor: Colors.blue,
//             tabs: <Widget>[
//               Tab(text: "下载列表"),
//               Tab(text: "上传列表"),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

class MyDownLoadPath extends StatefulWidget {
  const MyDownLoadPath({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return DownLoadPath();
  }
}

class DownLoadPath extends State {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: const Text(
        " 文件下载至:   Android/data/learning_space/files/Download/",
        textAlign: TextAlign.left,
        style: TextStyle(fontSize: 13),
      ),
      height: 18,
      decoration: BoxDecoration(color: Color.fromRGBO(217, 208, 208, 0.5)),
      alignment: Alignment.centerLeft,
    );
  }
}
