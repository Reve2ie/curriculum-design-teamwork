import 'dart:io';
import 'package:flutter/material.dart';
import '../bar.dart';
import 'dart:convert';
import 'package:animatedloginbutton/animatedloginbutton.dart';

class MyDialog extends StatelessWidget {
  showMyMaterialDialog(BuildContext context) {
    // print("_showMyMaterialDialog");
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            title: Container(
              child: Text("上传成功"),
            ),
            content: Container(child: Image.asset("images/success.jpg")),
            actions: <Widget>[
              new TextButton(
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.pop(context);
                },
                child: new Text("确认"),
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new FloatingActionButton(
        child: new Text("showDialog"),
        onPressed: () {
          showMyMaterialDialog(context);
        });
  }
}
