import 'dart:io';
import 'package:demo2/downloadpage/download.dart';
import 'package:demo2/searchpage/searchpage.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:convert';
import 'dialog.dart';
import 'package:animatedloginbutton/animatedloginbutton.dart';
import 'package:file_picker/file_picker.dart';
import 'package:percent_indicator/percent_indicator.dart';

class Upload extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return Uploadpage();
  }
}

class Uploadpage extends State {
  final LoginErrorMessageController loginErrorMessageController =
      LoginErrorMessageController();
  Future<bool> _successUpLoad() async {
    // 是否成功
    throw true;
  }

  selectFile() async {
    try {
      FilePickerResult? result = await FilePicker.platform.pickFiles();
      if (result != null) {
        File file = File(result.files.first.path.toString());
        return file;
      } else {
        // print("no");
      }
    } catch (e) {
      // print("______________________" + e.toString());
    }
  }

  double pro = 0.0;
  @override
  void initState() {
    super.initState();
    pro = 0.0;
  }

  var load = false;
  var file;
  var filename;
  var name = TextEditingController();
  var detail = TextEditingController();
  _postData(String name, String description, File file) async {
    //测试用
    // var dir = await getApplicationDocumentsDirectory();
    Dio dio = Dio();
    // File file = await new File('${dir}');
    Map<String, dynamic> map = Map();
    map["uploader"] = name.toString();
    map["description"] = description.toString();
    map["file"] = await MultipartFile.fromFile(file.path,
        filename: file.path.split('/').last.toString());

    ///通过FormData
    FormData formData = FormData.fromMap(map);
    //发送post
    load = false;
    Response response = await dio.post(
      'http://8.136.140.19:9000/temp/upload',
      data: formData,
      onSendProgress: (int progress, int total) {
        // ("当前进度是 $progress 总进度是 $total");

        setState(() {
          pro = progress / total;
        });
      },
    );

    ///服务器响应结果
    // if (response.statusCode == 200) print('上传成功！！！！！！！！');
    // var data = response.data;
    // print(data.toString());
    return response.statusCode;
  }

  var text = '上传成功';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            '感谢你出的每一份力',
          ),
          actions: [
            IconButton(
                tooltip: '查找',
                icon: const Icon(Icons.search),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) {
                      return SearchPage();
                    },
                  ));
                }),
            IconButton(
                tooltip: '传输列表',
                icon: const Icon(Icons.backup),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) {
                      return CardListScreen();
                    },
                  ));
                }),
          ],
        ),
        body: SingleChildScrollView(
            child: Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadiusDirectional.circular(20)),
                      clipBehavior: Clip.antiAlias,
                      child: Image.asset(
                        "images/R-C.jpg",
                        width: MediaQuery.of(context).size.width,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                        height: 40,
                        child: TextField(
                            onTap: () async {
                              file = await selectFile();
                              if (file != Null) {
                                setState(() {
                                  filename = file.path.split("/").last;
                                });
                              }
                            },
                            readOnly: true,
                            decoration: InputDecoration(
                                icon: Icon(Icons.local_library),
                                hintText: filename ?? "选择文件",
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                )))),
                    SizedBox(
                      height: 10,
                    ),
                    TextField(
                        controller: name,
                        maxLines: 1,
                        textAlign: TextAlign.start,
                        decoration: InputDecoration(
                            labelText: "您的大名,如果您不填的话就匿名哦",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(15.0),
                                borderSide: BorderSide(color: Colors.blue)))),
                    SizedBox(
                      height: 10,
                    ),
                    TextField(
                        controller: detail,
                        maxLines: 5,
                        textAlign: TextAlign.start,
                        decoration: InputDecoration(
                            labelText: "备注",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(15.0),
                                borderSide: BorderSide(color: Colors.blue)))),
                    const SizedBox(
                      height: 10,
                    ),
                    AnimatedLoginButton(
                      loginTip: "上传",
                      loginErrorMessageController: loginErrorMessageController,
                      textStyleError:
                          TextStyle(fontSize: 17, color: Colors.amber),
                      buttonColorError: Colors.red,
                      onTap: () async {
                        try {
                          // print(filename);
                          if (filename == null) {
                            loginErrorMessageController
                                .showErrorMessage("必须选择文件上传哦");
                          } else {
                            load = false;
                            setState(() {});
                            _postData(
                                    name.text.toString() == ''
                                        ? "匿名"
                                        : detail.text.toString(),
                                    detail.text.toString(),
                                    file)
                                .then((v) {
                              if (v == 200) {
                                MyDialog().showMyMaterialDialog(context);
                              }
                            });
                          }
                        } catch (Error) {
                          setState(() {
                            text = "网络异常";
                            loginErrorMessageController.showErrorMessage(text);
                          });
                        }
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    LinearPercentIndicator(
                      width: MediaQuery.of(context).size.width - 20,

                      lineHeight: 8.0,

                      percent: pro, //设置比例

                      // animation: true,

                      // animationDuration: 1000,

                      backgroundColor: Color.fromRGBO(238, 238, 238, 1),

                      linearGradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        colors: [
                          Color.fromRGBO(135, 206, 235, 0.1),
                          Color.fromRGBO(65, 105, 225, 1)
                        ],
                      ),
                    )
                  ],
                ))));
  }
}
