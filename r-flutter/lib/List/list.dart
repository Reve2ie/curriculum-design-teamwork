import 'dart:io';
import 'package:filesize/filesize.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:like_button/like_button.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import '../auto_refresh/auto_refresh.dart';

import 'package:permission_handler/permission_handler.dart';
import 'package:path_provider/path_provider.dart';
import 'package:dio/dio.dart';
import 'package:open_file/open_file.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../downloadpage/downloadserver.dart';

import 'dart:convert';

import 'package:demo2/tools/simple_storage.dart';

class LoaclServices {
  //插入新的记录
  static setHistoryData(String key, String value) async {
    try {
      await Storage.setString(key, value);
    } catch (e) {
      print("1:" + e.toString());
    }
  }

  //获取记录列表
  static getDonwloadList() async {
    try {
      var set = (await Storage.getAll());
      var list = [];
      var listmap = {};
      list.addAll(set);
      var Downloadlist = [];
      for (int i = 0; i < list.length; i++) {
        await Storage.getString(list[i]).then((value) {
          listmap = json.decode(value);
          Downloadlist.add({list[i]: listmap});
        });
      }
      return Downloadlist;
    } catch (e) {
      print("2:" + e.toString());
      return [];
    }
  }

  //删除所有记录
  static removeDonwload(String key) async {
    await Storage.remove(key);
  }
}

class MyDownloadList extends StatefulWidget {
  MyDownloadList({Key? key, required this.arguments}) : super(key: key);
  List<dynamic> arguments = [];
  @override
  State<StatefulWidget> createState() {
    return DownloadList(this.arguments);
  }
}

class DownloadList extends State {
  List<dynamic> arguments;
  DownloadList(this.arguments);
  var currentProgress = [];
  _checkstorage() async {
    if (await Permission.storage.request().isGranted) {
      return true;
    } else {
      return false;
    }
  }

  _findLocalPath() async {
    var Dir = Platform.isAndroid
        ? await getExternalStorageDirectory()
        : await getApplicationSupportDirectory();
    return Dir!.path;
  }

  _downloadFile(String url, String name, int index, String type,
      String formTpye, int size) async {
    var isPermission = await _checkstorage();
    var path = (await _findLocalPath()) + '/Download';
    if (isPermission) {
      File f = File(path + "/" + name);
      if (f.existsSync()) {
        currentProgress[index] = 1;
        setState(() {});
        OpenFile.open(f.path);
      } else {
        //print(path);
        Dio dio = Dio();
        dio.options.connectTimeout = 100000;
        dio.options.receiveTimeout = 100000;
        try {
          Response response = await dio.download(url, path + "/" + name,
              onReceiveProgress: (received, total) {
            if (total != -1) {
              //print(total);
              //print((received / 329728 * 100).toStringAsFixed(0));
              currentProgress[index] = received / total;
            } else {
              //print(total);
              Fluttertoast.showToast(
                msg: "下载失败,请稍后再试",
              );
            }
            setState(() {});
          });
          if (response.statusCode == 200) {
            // print("yes download");
            Fluttertoast.showToast(
              msg: name + "下载成功",
            );
            LoaclServices.setHistoryData(
                name.toString(),
                '{"fileName":"' +
                    name.toString() +
                    '","fileType":"' +
                    type.toString() +
                    '","fileSize":"' +
                    size.toString() +
                    '","Path":"' +
                    f.path.toString() +
                    '","fileFormat":"' +
                    formTpye +
                    '","data":"' +
                    DateTime.now().toString().split('.')[0] +
                    '"}');
          }
        } catch (e) {
          Fluttertoast.showToast(
            msg: "下载失败 " + e.toString(),
          );
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    for (int i = 0; i < arguments.length; i++) currentProgress.add(0);
    return AnimationLimiter(
        child: ListView.builder(
            padding: const EdgeInsets.all(8.0),
            itemCount: arguments.length,
            itemBuilder: (BuildContext context, int index) {
              return AnimationConfiguration.staggeredList(
                  position: index,
                  duration: const Duration(milliseconds: 375),
                  child: SlideAnimation(
                    verticalOffset: 44.0,
                    child: FadeInAnimation(
                        child: Card(
                            margin: const EdgeInsets.all(10),
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            shadowColor: Colors.white,
                            child: ListTile(
                              onTap: () => _downloadFile(
                                "http://8.136.140.19:9000/file/download/" +
                                    arguments[index]["uuid"],
                                arguments[index]["fileName"],
                                index,
                                arguments[index]["fileType"],
                                arguments[index]["fileFormat"],
                                arguments[index]["fileSize"],
                              ),
                              leading: Image.asset(
                                'images/file1.jpg',
                                fit: BoxFit.cover,
                              ),
                              title: Container(
                                  height: 93,
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          arguments[index]["fileName"],
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(fontSize: 13),
                                        ),
                                        Row(children: [
                                          Container(
                                              child: Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      4, 4, 4, 5),
                                                  child: Text(
                                                    arguments[index]["fileType"]
                                                        .toString(),
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 10),
                                                  )),
                                              decoration: BoxDecoration(
                                                  color: Colors.lightBlue,
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(
                                                              15)))),
                                          LikeButton(
                                            likeCount: arguments[index]
                                                ["likes"],
                                            uid: arguments[index]["uuid"],
                                            size: 18,
                                          ),
                                          Spacer(),
                                          Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  0, 5, 0, 0),
                                              child: CircularPercentIndicator(
                                                  radius: 25,
                                                  lineWidth: 4,
                                                  percent:
                                                      currentProgress[index] +
                                                          0.0,
                                                  center: new Icon(
                                                    Icons.download,
                                                    size: 15,
                                                    color: Colors.blue,
                                                  ),
                                                  backgroundColor: Colors.grey,
                                                  progressColor: Colors.blue,
                                                  footer: Text(
                                                    " " +
                                                        (currentProgress[
                                                                    index] *
                                                                100)
                                                            .truncateToDouble()
                                                            .toString() +
                                                        "%",
                                                    style:
                                                        TextStyle(fontSize: 10),
                                                  ))),
                                        ]),
                                        Text(
                                          "详情:\n   " +
                                              arguments[index]["description"],
                                          style: TextStyle(fontSize: 10),
                                          textAlign: TextAlign.start,
                                          maxLines: 3,
                                          overflow: TextOverflow.ellipsis,
                                        )
                                      ])),
                              subtitle: Container(
                                  width: 80,
                                  child: Text(
                                    "上传者: " +
                                        arguments[index]["provider"]
                                            .toString() +
                                        "   " +
                                        arguments[index]["updateDate"]
                                            .toString()
                                            .split('T')[0] +
                                        "  Size:" +
                                        filesize(arguments[index]["fileSize"]
                                            .toString()),
                                    maxLines: 1,
                                    // overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontSize: 10),
                                  )),
                              tileColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ))),
                  ));
            }));
  }
}
