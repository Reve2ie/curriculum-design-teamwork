import 'dart:io';
import 'package:demo2/downloadpage/downloadserver.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:like_button/like_button.dart';
import 'package:open_file/open_file.dart';
import '../auto_refresh/auto_refresh.dart';
import 'package:filesize/filesize.dart';
import '../tools/file_type_icon.dart';

import 'dart:convert';

import 'package:demo2/tools/simple_storage.dart';

class LoaclServices {
  //插入新的记录
  static setHistoryData(String key, String value) async {
    try {
      await Storage.setString(key, value);
    } catch (e) {
      print("1:" + e.toString());
    }
  }

  //获取记录列表
  static getDonwloadList() async {
    try {
      var set;
      await Storage.getAll().then((value) {
        value.remove("hotSearchList");
        set = value;
      });
      var list = [];
      var listmap = {};
      list.addAll(set);
      var Downloadlist = [];
      for (int i = 0; i < list.length; i++) {
        await Storage.getString(list[i]).then((value) {
          listmap = json.decode(value);
          Downloadlist.add({list[i]: listmap});
        });
      }
      return Downloadlist;
    } catch (e) {
      print("2:" + e.toString());
      return [];
    }
  }

  //删除所有记录
  static removeDonwload(String key) async {
    await Storage.remove(key);
  }
}

class MyLoaclList extends StatefulWidget {
  MyLoaclList({Key? key, required this.arguments}) : super(key: key);
  List<dynamic> arguments;

  @override
  State<StatefulWidget> createState() {
    return LoaclList(this.arguments);
  }
}

class LoaclList extends State {
  List<dynamic> arguments;
  LoaclList(this.arguments);
  @override
  void initState() {
    super.initState();
    //print("arguments:");
    //print(arguments[0][arguments[0].keys.toString().split('(')[1].split(')')[0]]
    //    ["fileName"]);
  }

  @override
  Widget build(BuildContext context) {
    TextEditingController _controller = TextEditingController();
    return AnimationLimiter(
        child: ListView.builder(
            padding: const EdgeInsets.all(8.0),
            itemCount: arguments.length,
            itemBuilder: (BuildContext context, int index) {
              return AnimationConfiguration.staggeredList(
                  position: index,
                  duration: const Duration(milliseconds: 375),
                  child: SlideAnimation(
                      verticalOffset: 50.0,
                      child: FadeInAnimation(
                          child: Card(
                              margin: const EdgeInsets.all(8),
                              elevation: 2,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              shadowColor: Colors.white,
                              child: ListTile(
                                leading: arguments[index][arguments[index]
                                            .keys
                                            .toString()
                                            .split('(')[1]
                                            .split(')')[0]]["fileFormat"] ==
                                        'doc'
                                    ? FileTypeIcon(FileType.msWord)
                                    : FileTypeIcon(FileType.msPowerPoint),
                                title: Text(
                                  arguments[index][arguments[index]
                                      .keys
                                      .toString()
                                      .split('(')[1]
                                      .split(')')[0]]["fileName"],
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(fontSize: 15),
                                ),
                                subtitle: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Size: " +
                                            filesize(arguments[index][
                                                    arguments[index]
                                                        .keys
                                                        .toString()
                                                        .split('(')[1]
                                                        .split(')')[0]]
                                                ["fileSize"]),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(fontSize: 10),
                                      ),
                                      Text(
                                        "下载日期: " +
                                            arguments[index][arguments[index]
                                                .keys
                                                .toString()
                                                .split('(')[1]
                                                .split(')')[0]]["data"],
                                        style: TextStyle(fontSize: 10),
                                      )
                                    ]),
                                tileColor: Colors.white,
                                onTap: () {
                                  OpenFile.open(arguments[index][
                                      arguments[index]
                                          .keys
                                          .toString()
                                          .split('(')[1]
                                          .split(')')[0]]["Path"]);
                                },
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                trailing: IconButton(
                                  iconSize: 35,
                                  color: Colors.red[300],
                                  onPressed: () {
                                    try {
                                      File f = File(arguments[index][
                                          arguments[index]
                                              .keys
                                              .toString()
                                              .split('(')[1]
                                              .split(')')[0]]["Path"]);
                                      f.delete();
                                      LoaclServices.removeDonwload(
                                          arguments[index][arguments[index]
                                              .keys
                                              .toString()
                                              .split('(')[1]
                                              .split(')')[0]]["fileName"]);
                                      LoaclServices.getDonwloadList().then((v) {
                                        arguments = v;
                                        Fluttertoast.showToast(
                                          msg: "成功删除!",
                                        );
                                        setState(() {});
                                      });
                                      ;
                                    } catch (e) {
                                      print(e);
                                    }
                                  },
                                  icon: Icon(Icons.local_drink),
                                ),
                              )))));
            }));
  }
}
