import 'dart:io';
import 'package:filesize/filesize.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:like_button/like_button.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import '../auto_refresh/auto_refresh.dart';
import 'package:http/http.dart' as http;

import 'package:permission_handler/permission_handler.dart';
import 'package:path_provider/path_provider.dart';
import 'package:dio/dio.dart';
import 'package:open_file/open_file.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../downloadpage/downloadserver.dart';

import 'dart:convert';

import 'package:demo2/tools/simple_storage.dart';

class LoaclServices {
  //插入新的记录
  static setHistoryData(String key, String value) async {
    try {
      await Storage.setString(key, value);
    } catch (e) {
      print("1:" + e.toString());
    }
  }

  //获取记录列表
  static getDonwloadList() async {
    try {
      var set;
      await Storage.getAll().then((value) {
        value.remove("hotSearchList");
        set = value;
        //print('_____________');
        //print(set);
        //print('_____________');
      });
      var list = [];
      var listmap = {};
      list.addAll(set);
      var Downloadlist = [];
      for (int i = 0; i < list.length; i++) {
        await Storage.getString(list[i]).then((value) {
          listmap = json.decode(value);
          Downloadlist.add({list[i]: listmap});
        });
      }
      return Downloadlist;
    } catch (e) {
      print("2:" + e.toString());
      return [];
    }
  }

  //删除所有记录
  static removeDonwload(String key) async {
    await Storage.remove(key);
  }
}

class MySearchList extends StatefulWidget {
  MySearchList({Key? key, required this.arguments}) : super(key: key);
  List<dynamic> arguments = [];
  @override
  State<StatefulWidget> createState() {
    return SearchList(this.arguments);
  }
}

class SearchList extends State {
  List<dynamic> arguments;
  var imagelist = [];
  SearchList(this.arguments);
  var currentProgress = [];
  _checkstorage() async {
    if (await Permission.storage.request().isGranted) {
      return true;
    } else {
      return false;
    }
  }

  _findLocalPath() async {
    var Dir = Platform.isAndroid
        ? await getExternalStorageDirectory()
        : await getApplicationSupportDirectory();
    return Dir!.path;
  }

  _downloadFile(String url, String name, int index, String type,
      String formTpye, int size) async {
    var isPermission = await _checkstorage();
    var path = (await _findLocalPath()) + '/Download';
    if (isPermission) {
      File f = File(path + "/" + name);
      //(f.path);
      if (f.existsSync()) {
        currentProgress[index] = 1;
        setState(() {});
        OpenFile.open(f.path);
      } else {
        //print(path);
        Dio dio = Dio();
        dio.options.connectTimeout = 100000;
        dio.options.receiveTimeout = 100000;
        try {
          Response response = await dio.download(url, path + "/" + name,
              onReceiveProgress: (received, total) {
            if (total != -1) {
              //print(total);
              //print((received / 329728 * 100).toStringAsFixed(0));
              currentProgress[index] = received / total;
            } else {
              //print(total);
              Fluttertoast.showToast(
                msg: "下载失败,请稍后再试",
              );
            }
            setState(() {});
          });
          if (response.statusCode == 200) {
            //print("yes download");
            Fluttertoast.showToast(
              msg: name + "下载成功",
            );
            LoaclServices.setHistoryData(
                name.toString(),
                '{"fileName":"' +
                    name.toString() +
                    '","fileType":"' +
                    type.toString() +
                    '","fileSize":"' +
                    size.toString() +
                    '","Path":"' +
                    f.path.toString() +
                    '","fileFormat":"' +
                    formTpye +
                    '","data":"' +
                    DateTime.now().toString().split('.')[0] +
                    '"}');
          }
        } catch (e) {
          Fluttertoast.showToast(
            msg: "下载失败 " + e.toString(),
          );
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    for (int i = 0; i < arguments.length; i++) currentProgress.add(0);
    return AnimationLimiter(
        child: ListView.builder(
            padding: const EdgeInsets.all(8.0),
            itemCount: arguments.length,
            itemBuilder: (BuildContext context, int index) {
              return AnimationConfiguration.staggeredList(
                  position: index,
                  duration: const Duration(milliseconds: 375),
                  child: SlideAnimation(
                      verticalOffset: 44.0,
                      child: FadeInAnimation(
                        child: Container(
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                        color: Colors.grey, width: 0.1))),
                            height: 100,
                            child: InkWell(
                                onTap: () => _downloadFile(
                                      "http://8.136.140.19:9000/file/download/" +
                                          arguments[index]["uuid"],
                                      arguments[index]["fileName"],
                                      index,
                                      arguments[index]["fileType"],
                                      arguments[index]["fileFormat"],
                                      arguments[index]["fileSize"],
                                    ),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        child: Image.network(
                                          'http://8.136.140.19:9000/file/pic/' +
                                              arguments[index]["uuid"],
                                        ),
                                        padding:
                                            EdgeInsets.fromLTRB(0, 2, 0, 2),
                                      ),
                                      flex: 4,
                                    ),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Expanded(
                                              child: Text(
                                                arguments[index]["fileName"],
                                                textAlign: TextAlign.left,
                                                overflow: TextOverflow.fade,
                                                maxLines: 1,
                                                style: TextStyle(fontSize: 14),
                                              ),
                                              flex: 2),
                                          Expanded(
                                            child: Row(children: [
                                              Container(
                                                  height: 25,
                                                  decoration: BoxDecoration(
                                                      color: Colors.lightBlue,
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  15))),
                                                  padding: EdgeInsets.fromLTRB(
                                                      8, 4, 8, 4),
                                                  child: Text(
                                                    arguments[index]
                                                        ['fileType'],
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 10),
                                                  )),
                                              LikeButton(
                                                likeCount: arguments[index]
                                                    ["likes"],
                                                uid: arguments[index]["uuid"],
                                                size: 25,
                                              ),
                                              Spacer(),
                                              Row(
                                                children: [
                                                  CircularPercentIndicator(
                                                    radius: 22,
                                                    lineWidth: 4,
                                                    percent:
                                                        currentProgress[index] +
                                                            0.0,
                                                    center: new Icon(
                                                      Icons.download,
                                                      size: 15,
                                                      color: Colors.blue,
                                                    ),
                                                    backgroundColor:
                                                        Colors.grey,
                                                    progressColor: Colors.blue,
                                                  ),
                                                  Text(
                                                    (currentProgress[index] ==
                                                            1)
                                                        ? "100%"
                                                        : " " +
                                                            (currentProgress[
                                                                        index] *
                                                                    100)
                                                                .toInt()
                                                                .toString() +
                                                            "%",
                                                    style:
                                                        TextStyle(fontSize: 13),
                                                  )
                                                ],
                                              ),
                                            ]),
                                            flex: 2,
                                          ),
                                          Expanded(
                                            child: Padding(
                                                padding: EdgeInsets.fromLTRB(
                                                    0, 0, 0, 4),
                                                child: Text(
                                                  "备注:\n   " +
                                                      arguments[index]
                                                          ["description"],
                                                  textAlign: TextAlign.left,
                                                  style:
                                                      TextStyle(fontSize: 10),
                                                  maxLines: 3,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                )),
                                            flex: 4,
                                          ),
                                          Expanded(
                                            child: Text(
                                              "上传者: " +
                                                  arguments[index]["provider"] +
                                                  "  日期:" +
                                                  arguments[index]
                                                      ["updateDate"] +
                                                  "   Size:" +
                                                  filesize(arguments[index]
                                                          ["fileSize"]
                                                      .toString()),
                                              style: TextStyle(
                                                  fontSize: 7,
                                                  color: Colors.grey[350]),
                                              overflow: TextOverflow.fade,
                                            ),
                                            flex: 1,
                                          )
                                        ],
                                      ),
                                      flex: 5,
                                    ),
                                  ],
                                ))),
                      )));
            }));
  }
}
