import 'dart:io';
import 'package:filesize/filesize.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

import 'package:permission_handler/permission_handler.dart';
import 'package:path_provider/path_provider.dart';
import 'package:dio/dio.dart';
import 'package:open_file/open_file.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:like_button/like_button.dart';
import 'package:flutter/cupertino.dart';
import '../downloadpage/downloadserver.dart';

// import 'package:path_provider/path_provider.dart';
// import 'package:permission_handler/permission_handler.dart';
import 'package:toast/toast.dart';
import 'package:open_file/open_file.dart';

import 'dart:convert';

import 'package:demo2/tools/simple_storage.dart';

class LoaclServices {
  //插入新的记录
  static setHistoryData(String key, String value) async {
    try {
      await Storage.setString(key, value);
    } catch (e) {
      print("1:" + e.toString());
    }
  }

  //获取记录列表
  static getDonwloadList() async {
    try {
      var set = (await Storage.getAll());
      var list = [];
      var listmap = {};
      list.addAll(set);
      var Downloadlist = [];
      for (int i = 0; i < list.length; i++) {
        await Storage.getString(list[i]).then((value) {
          listmap = json.decode(value);
          Downloadlist.add({list[i]: listmap});
        });
      }
      return Downloadlist;
    } catch (e) {
      print("2:" + e.toString());
      return [];
    }
  }

  //删除所有记录
  static removeDonwload(String key) async {
    await Storage.remove(key);
  }
}

class CardGridList extends StatefulWidget {
  CardGridList({Key? key, required this.arguments}) : super(key: key);
  List<Map<dynamic, dynamic>> arguments = [];
  @override
  CardGridListState createState() => CardGridListState(this.arguments);
}

class CardGridListState extends State {
  var currentProgress = [];
  List<Map> arguments;
  CardGridListState(this.arguments);
  _checkstorage() async {
    if (await Permission.storage.request().isGranted) {
      return true;
    } else {
      return false;
    }
  }

  _findLocalPath() async {
    var Dir = Platform.isAndroid
        ? await getExternalStorageDirectory()
        : await getApplicationSupportDirectory();
    return Dir!.path;
  }

  _downloadFile(String url, String name, int index, String type,
      String formTpye, int size) async {
    var isPermission = await _checkstorage();
    var path = (await _findLocalPath()) + '/Download';
    if (isPermission) {
      File f = File(path + "/" + name);
      if (f.existsSync()) {
        currentProgress[index] = 1;
        setState(() {});
        OpenFile.open(f.path);
      } else {
        // print(path);
        Dio dio = Dio();
        dio.options.connectTimeout = 100000;
        dio.options.receiveTimeout = 100000;
        try {
          Response response = await dio.download(url, path + "/" + name,
              onReceiveProgress: (received, total) {
            if (total != -1) {
              // print(total);
              // print((received / 329728 * 100).toStringAsFixed(0));
              currentProgress[index] = received / total;
            } else {
              // print(total);
              Fluttertoast.showToast(
                msg: "下载失败,请稍后再试",
              );
            }
            setState(() {});
          });
          if (response.statusCode == 200) {
            // print("yes download");
            Fluttertoast.showToast(
              msg: name + "下载成功",
            );
            LoaclServices.setHistoryData(
                name.toString(),
                '{"fileName":"' +
                    name.toString() +
                    '","fileType":"' +
                    type.toString() +
                    '","fileSize":"' +
                    size.toString() +
                    '","Path":"' +
                    f.path.toString() +
                    '","fileFormat":"' +
                    formTpye +
                    '","data":"' +
                    DateTime.now().toString().split('.')[0] +
                    '"}');
          }
        } catch (e) {
          Fluttertoast.showToast(
            msg: "下载失败 " + e.toString(),
          );
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    // print("________________________________________");
    // print(arguments[0]["likes"]);
    var columnCount = 2;
    for (int i = 0; i < arguments.length; i++) currentProgress.add(0);
    return AnimationLimiter(
      child: GridView.count(
        cacheExtent: 10,
        childAspectRatio: 1.0,
        padding: const EdgeInsets.all(8.0),
        crossAxisCount: columnCount,
        children: List.generate(
          arguments.length,
          (int index) {
            return AnimationConfiguration.staggeredGrid(
              columnCount: columnCount,
              position: index,
              duration: const Duration(milliseconds: 375),
              child: ScaleAnimation(
                  scale: 0.5,
                  child: FadeInAnimation(
                    child: Padding(
                        padding: EdgeInsets.all(15),
                        child: InkWell(
                          onTap: () {
                            _downloadFile(
                              "http://8.136.140.19:9000/file/download/" +
                                  arguments[index]["uuid"],
                              arguments[index]["fileName"],
                              index,
                              arguments[index]["fileType"],
                              arguments[index]["fileFormat"],
                              arguments[index]["fileSize"],
                            );
                          },
                          child: Container(
                            height: 170,
                            width: 200,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: .2,
                                  blurRadius: 5,
                                  offset: Offset(0, 5),
                                ),
                              ],
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Row(
                                    children: [
                                      Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(8, 8, 0, 0),
                                        child: Image.asset(
                                          "images/file2.jpg",
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      Spacer(),
                                      Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(0, 10, 0, 0),
                                        child: Column(
                                          children: [
                                            Container(
                                              child: Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      4, 1, 4, 3),
                                                  child: Text(
                                                    arguments[index]["fileType"]
                                                        .toString(),
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 10),
                                                  )),
                                              decoration: BoxDecoration(
                                                  color: Colors.lightBlue,
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(15))),
                                            ),
                                            LikeButton(
                                              uid: arguments[index]["uuid"],
                                              likeCount: arguments[index]
                                                  ["likes"],
                                              size: 20,
                                            ),
                                            Row(
                                              children: [
                                                CircularPercentIndicator(
                                                  radius: 15,
                                                  lineWidth: 2,
                                                  percent:
                                                      currentProgress[index] +
                                                          0.0,
                                                  center: new Icon(
                                                    Icons.download,
                                                    size: 10,
                                                    color: Colors.blue,
                                                  ),
                                                  backgroundColor: Colors.grey,
                                                  progressColor: Colors.blue,
                                                ),
                                                Text(
                                                  " " +
                                                      (currentProgress[index] *
                                                              100)
                                                          .truncateToDouble()
                                                          .toString() +
                                                      "%",
                                                  style:
                                                      TextStyle(fontSize: 10),
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                  flex: 4,
                                ),
                                Expanded(
                                  child: Padding(
                                      padding: EdgeInsets.fromLTRB(8, 0, 8, 8),
                                      child: ListTile(
                                        title: Text(
                                          arguments[index]["fileName"],
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(fontSize: 15),
                                        ),
                                        subtitle: Text(
                                          '详情:\n   ' +
                                              arguments[index]["description"],
                                          style: TextStyle(fontSize: 8),
                                        ),
                                        contentPadding: const EdgeInsets.all(0),
                                      )),
                                  flex: 5,
                                ),
                                Expanded(
                                  child: Container(
                                    width: 200,
                                    height: 170,
                                    child: Text(
                                        "     上传者:" +
                                            arguments[index]["provider"]
                                                .toString() +
                                            "  " +
                                            arguments[index]["updateDate"]
                                                .toString() +
                                            "          Size:" +
                                            filesize(arguments[index]
                                                    ["fileSize"]
                                                .toString()),
                                        style: TextStyle(
                                            color: Colors.grey, fontSize: 6),
                                        textAlign: TextAlign.start),
                                    decoration: BoxDecoration(
                                        border: Border(
                                            top: BorderSide(
                                                width: 1, color: Colors.blue))),
                                  )
                                  // _typeIcons(fileType),
                                  ,
                                  flex: 1,
                                )
                              ],
                            ),
                          ),
                        )),
                  )),
            );
          },
        ),
      ),
    );
  }
}
