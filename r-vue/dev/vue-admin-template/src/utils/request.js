import axios from 'axios'
import { Message } from 'element-ui'
import store from '@/store'


// create an axios instance
const service = axios.create({
  baseURL: 'http://localhost:8080',
  // baseURL: 'http://8.136.140.19:9000/',
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000, // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent

    if (localStorage.getItem('token')) {
      // let each request carry token
      // ['Authorization'] is a custom headers key
      // please modify it according to the actual situation
      config.headers['Authorization'] = localStorage.getItem('token')
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data

    if (res.code === "0"){
      return response
    } else {
      // 弹窗异常信息
      Message({
        message: response.data.msg,
        type: 'error',
        duration: 5 * 1000
      })

    /*
      // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
      if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
        // to re-login
        MessageBox.confirm('You have been logged out, you can cancel to stay on this page, or log in again', 'Confirm logout', {
          confirmButtonText: 'Re-Login',
          cancelButtonText: 'Cancel',
          type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            location.reload()
          })
        })
      }
      return Promise.reject(new Error(res.message || 'Error'))
    } else {
      return res
    } */

    return Promise.reject(response.data.msg)
  }

  },
  error => {
    console.log('err' + error) // for debug
     if(error.response.data) {
            error.message = error.response.data.msg
          }
      // 根据请求状态觉得是否登录或者提示其他
      if (error.response.status === 401) {
        store.dispatch('user/resetToken');
        router.push({
          path: '/login'
        });
        error.message = '请重新登录';
      }
      if (error.response.status === 403) {
        error.message = '权限不足，无法访问';
      }
      if(error.response.status === 500){
        error.message = 'token已经失效，请重新登录！'
        store.dispatch('user/resetToken');
        router.push({
          path: '/login'
        });
      }
      
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
