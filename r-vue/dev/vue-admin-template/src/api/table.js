import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/temp/search/all',
    method: 'get',
    params
  })
}

export function getFileList(params){
  return request({
    url: '/file/search/all',
    method: 'get',
    params
  })
}

export function getTypeList(params){
  return request({
    url: '/type/search/all',
    method: 'get',
    params
  })
}

export function getAllType(){
  return request({
    url: '/type/all',
    method: 'get',
  })
}

export function getUserList(params){
  return request({
    url: '/user/search/all',
    method: 'get',
    params
  })
}

export function getDatInfo(){
  return request({
    url: '/data/dat-info',
    method: "get"
  })
}

export function getDalInfo(){
  return request({
    url: '/data/dal-info',
    method: "get"
  })
}

export function getTopTypes(){
  return request({
    url: '/data/top-types',
    method: "get"
  })
}

export function getTopFiles(){
  return request({
    url: '/data/top-files',
    method: "get"
  })
}