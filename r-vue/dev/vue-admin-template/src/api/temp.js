import request from '@/utils/request'

/* 上传临时文件  */
export function upload(data) {
    return request({
      url: '/temp/upload',
      method: 'post',
      data
    })
}

/* 移动临时文件 */
export function remove(data) {
  return request({
    url: '/temp/remove',
    method: 'post',
    data
  })
}

/* 删除临时文件 */
export function deleteFile(params) {
  return request({
    url: '/temp/delete',
    method: 'post',
    params
  })
}