import request from '@/utils/request'

export function searchFile(params) {
  return request({
    url: '/file/search/operation',
    method: 'get',
    params
  })
}

export function searchTempFile(params) {
  return request({
    url: '/temp/search/type',
    method: 'get',
    params
  })
}

export function searchType(params) {
  return request({
    url: '/type/search/type',
    method: 'get',
    params
  })
}