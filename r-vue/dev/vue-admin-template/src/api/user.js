import request from '@/utils/request'

/* 登录 */
export function login(data) {
  return request({
    url: 'user/login',
    method: 'post',
    data
  })
}

/* 搜索 */
export function searchUser(params) {
  return request({
    url: '/user/search/type',
    method: 'get',
    params
  })
}

/* 添加用户 */
export function addUser(data) {
  return request({
    url: '/user/add',
    method: 'post',
    data
  })
}

/* 添加头像 */
export function addAvatar(data) {
  return request({
    url: '/user/add/avatar',
    method: 'post',
    data
  })
}

/* 更改密码 */
export function changePassword(params) {
  return request({
    url: '/user/updatePwd',
    method: 'post',
    params
  })
}

/* 编辑用户 */
export function updateUser(params) {
  return request({
    url: '/user/update',
    method: 'post',
    params
  })
}

/* 删除用户 */
export function deleteUser(params) {
  return request({
    url: '/user/delete',
    method: 'post',
    params
  })
}

/* 退出 */
export function logout() {
  return request({
    url: 'user/logout',
    method: 'get'
  })
}
