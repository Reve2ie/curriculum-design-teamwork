import request from '@/utils/request'

/*更新文件信息  */
export function update(params) {
    return request({
      url: '/file/update',
      method: 'post',
      params
    })
}

/* 删除文件信息 */
export function deleteFile(params) {
    return request({
      url: '/file/delete',
      method: 'post',
      params
    })
}