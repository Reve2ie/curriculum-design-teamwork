import request from '@/utils/request'

/* 新增类型 */
export function add(params) {
    return request({
      url: '/type/add',
      method: 'post',
      params
    })
}

/* 更新类型 */
export function update(params) {
    return request({
      url: '/type/update',
      method: 'post',
      params
    })
}

/* 删除类型 */
export function deleteType(params) {
    return request({
      url: '/type/delete',
      method: 'post',
      params
    })
}
