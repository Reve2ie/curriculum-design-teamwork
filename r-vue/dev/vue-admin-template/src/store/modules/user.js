import { login, logout } from '@/api/user'
import router, { resetRouter } from '@/router'
import store from '..'

const getDefaultState = () => {
  return {
    token: '',
    userInfo: {},
    name: '',
    avatar: '',
    roles: []
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    localStorage.removeItem("token")
    localStorage.removeItem("userInfo")
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    localStorage.setItem("token",token)
    state.token = token
  },
  SET_USERINFO: (state, userInfo) => {
    state.userInfo = userInfo
    localStorage.setItem("userInfo", JSON.stringify(userInfo))
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {
      login({ loginName: username.trim(), password: password }).then(response => {
        const token = response.headers['authorization']
        commit('SET_TOKEN', token)
        commit('SET_USERINFO', response.data.data)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      /* getInfo(state.token).then(response => {
        const { data } = response

        if (!data) {
          return reject('Verification failed, please Login again.')
        }

        const { name, avatar } = data

        commit('SET_NAME', name)
        commit('SET_AVATAR', avatar)
        resolve(data)
      }).catch(error => {
        reject(error)
      }) */

      const data = JSON.parse(localStorage.getItem("userInfo"))
      if (!data) {
        return reject('Verification failed, please Login again.')
      }

      let roles = []
      roles.push(data.role)
      // roles must be a non-empty array
      if (!roles || roles.length <= 0) {
        reject('getInfo: roles must be a non-null array!')
      }

      commit('SET_USERINFO', data)
      commit('SET_NAME', data.userName)
      commit('SET_AVATAR', data.avatar)
      commit('SET_ROLES',roles)
      resolve(data)
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      logout(state.token).then(() => {
        resetRouter()
        commit('RESET_STATE')
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

