package com.reverie.common;

import lombok.Data;

/**
 * 统一结果封装
 * */
@Data
public class Result {
    private String code;
    private String msg;
    private Object data;

    public static Result success(String msg,Object data){
        Result r = new Result();
        r.setCode("0");
        r.setMsg(msg);
        r.setData(data);
        return r;
    }

    public static Result success(Object data){
        return success("操作成功",data);
    }

    public static Result fail(String code,String msg,Object data){
        Result r = new Result();
        r.setCode(code);
        r.setMsg(msg);
        r.setData(data);
        return r;
    }

    public static Result fail(String code,String msg){
        return fail(code,msg,null);
    }

    public static Result fail(String msg){
        return fail("-1",msg,null);
    }
}
