package com.reverie.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Reverie
 * @since 2021-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysFile implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "file_id", type = IdType.AUTO)
    private Long fileId;

    private String fileName;

    private String fileFormat;

    private Long fileSize;

    private String fileType;

    private String provider;

    private LocalDateTime updateDate;

    private LocalDateTime addDate;

    private String uuid;

    private Integer downloads;

    private Integer likes;

    private String description;

    private String status;

}
