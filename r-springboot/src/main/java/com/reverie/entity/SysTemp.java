package com.reverie.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Reverie
 * @since 2021-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysTemp implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "temp_id", type = IdType.AUTO)
    private Long tempId;

    private String fileName;

    private Long fileSize;

    private String uploader;

    private String description;

    private String uuid;

    private LocalDateTime gmtCreate;


}
