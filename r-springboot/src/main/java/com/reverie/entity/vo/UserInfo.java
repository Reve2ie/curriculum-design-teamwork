package com.reverie.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *  返回给前端用户消息对象
 * </p>
 *
 * @author Reverie
 * @since 2021-09-14
 */
@Data
public class UserInfo implements Serializable {
    private String loginName;
    private String userName;
    private String avatar;
    private String role;
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDateTime loginDate;
}
