package com.reverie.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author Reverie
 * @since 2021-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysDaily implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "daily_id", type = IdType.AUTO)
    private Long dailyId;

    private LocalDate dailyDate;

    private Integer dailyDownloads;

    private Integer dailyLike;
}
