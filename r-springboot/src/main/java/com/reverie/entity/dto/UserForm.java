package com.reverie.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 *  前端表单对象
 * </p>
 *
 * @author Reverie
 * @since 2021-09-14
 */
@Data
public class UserForm implements Serializable {
    @NotBlank(message = "账号不能为空")
    private String loginName;

    @NotBlank(message = "昵称不能为空")
    private String userName;

    @NotBlank(message = "密码不能为空")
    private String password;

    @NotBlank(message = "角色权限不能为空")
    private String role;

    private Integer status;
}
