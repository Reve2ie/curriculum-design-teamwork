package com.reverie.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 *  前端登录对象
 * </p>
 *
 * @author Reverie
 * @since 2021-09-14
 */
@Data
public class LoginUser implements Serializable {
    @NotBlank(message = "账号不能为空")
    private String loginName;
    @NotBlank(message = "密码不能为空")
    private String password;
}
