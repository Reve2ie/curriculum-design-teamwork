package com.reverie.service.impl;

import com.reverie.entity.SysTemp;
import com.reverie.mapper.SysTempMapper;
import com.reverie.service.SysTempService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Reverie
 * @since 2021-09-14
 */
@Service
public class SysTempServiceImpl extends ServiceImpl<SysTempMapper, SysTemp> implements SysTempService {

}
