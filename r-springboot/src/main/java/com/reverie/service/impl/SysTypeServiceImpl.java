package com.reverie.service.impl;

import com.reverie.entity.SysType;
import com.reverie.mapper.SysTypeMapper;
import com.reverie.service.SysTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Reverie
 * @since 2021-09-14
 */
@Service
public class SysTypeServiceImpl extends ServiceImpl<SysTypeMapper, SysType> implements SysTypeService {

}
