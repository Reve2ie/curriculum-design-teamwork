package com.reverie.service;

import com.reverie.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Reverie
 * @since 2021-09-14
 */
public interface SysUserService extends IService<SysUser> {

}
