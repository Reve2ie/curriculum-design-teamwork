package com.reverie.service;

import com.reverie.entity.SysType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Reverie
 * @since 2021-09-14
 */
public interface SysTypeService extends IService<SysType> {

}
