package com.reverie.config;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.reverie.entity.SysDaily;
import com.reverie.mapper.SysDailyMapper;
import com.reverie.util.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

@Configuration
@EnableScheduling
public class ScheduleTaskConfig implements SchedulingConfigurer {

    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private SysDailyMapper dailyMapper;

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setScheduler(taskScheduler());
        taskRegistrar.addTriggerTask(
                //执行定时任务
                () ->
                        System.out.println("执行动态定时任务: " + LocalDateTime.now().toLocalTime()),
                //设置触发器
                triggerContext -> {

                    // String cron = "0/10 * * * * ?";//获取定时任务周期表达式 每10s
                    String cron = "0 0 1 * * ?"; // 每天凌晨1点

                    CronTrigger trigger = new CronTrigger(cron);

                    return trigger.nextExecutionTime(triggerContext);
                }

        );
    }


    @Bean
    public Executor taskScheduler() {
        //设置线程名称
        ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().setNameFormat("demo-pool-%d").build();
        //创建线程池
        return Executors.newScheduledThreadPool(5, namedThreadFactory);
    }

    // 每日定时任务 存储每日下载量与like度
    public void taskService(){
        int downloads = redisUtils.get("dailyDownloads") == null ? 0 : Integer.parseInt(redisUtils.get("dailyDownloads"));
        int like = redisUtils.get("dailyLike") == null ? 0 : Integer.parseInt(redisUtils.get("dailyLike"));

        SysDaily daily = new SysDaily();
        daily.setDailyDate(LocalDate.now());
        daily.setDailyDownloads(downloads);
        daily.setDailyLike(like);
        dailyMapper.insert(daily);

        redisUtils.delete("dailyDownloads");
        redisUtils.delete("dailyLike");
    }
}



