package com.reverie.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * 全局配置类
 *
 * @author reverie
 */

@Configuration
@Component
@ConfigurationProperties(prefix = "reverie")
public class ReverieConfig {

    // 项目名称
    public static String name;

    // 版本
    public static String version;

    // 年份
    private static String copyrightYear;

    // 基本路径
    private static String profile;

    public static String getName() {
        return name;
    }

    public void setName(String name) {
        ReverieConfig.name = name;
    }

    public static String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        ReverieConfig.version = version;
    }

    public static String getCopyrightYear() {
        return copyrightYear;
    }

    public void setCopyrightYear(String copyrightYear) {
        ReverieConfig.copyrightYear = copyrightYear;
    }

    public static String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        ReverieConfig.profile = profile;
    }

    // 获取缓冲区路径
    public static String getTempPath(){
        return getProfile() + "/upload/";
    }

    // 获取文件路径
    public static String getFilePath(){
        return getProfile() + "/warehouse/";
    }

    // 获取图片路径
    public static String getPicPath(){
        return getProfile() + "/pic/";
    }

    // 获取头像路径
    public static String getAvatarPath()
    {
        return getProfile() + "/avatar/";
    }

}
