package com.reverie.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
@EnableOpenApi
public class SwaggerConfig {

    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .enable(true)
                .groupName("GZL")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.reverie.controller"))
                .paths(PathSelectors.ant("/**"))
                .build();
    }

    @SuppressWarnings("all")
    public ApiInfo apiInfo(){
        return new ApiInfo(
                "Reverie's api",
                "Team Project",
                "v1.0",
                "1078990940@qq.com",
                "GZL",
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0"
        );
    }
}
