package com.reverie.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.reverie.common.Result;
import com.reverie.config.ReverieConfig;
import com.reverie.entity.SysUser;
import com.reverie.entity.dto.LoginUser;
import com.reverie.entity.dto.UserForm;
import com.reverie.entity.vo.UserInfo;
import com.reverie.service.SysUserService;
import com.reverie.shiro.AccountProfile;
import com.reverie.util.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDateTime;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Reverie
 * @since 2021-09-14
 */
@Slf4j
@RestController
@RequestMapping("/user")
@CrossOrigin
@ApiIgnore
public class SysUserController {

    @Autowired
    SysUserService userService;
    @Autowired
    JwtUtils jwtUtils;

    private final static String BaseAvatarPath = ReverieConfig.getAvatarPath();

    // 账号信息
    @GetMapping("/search/all")
    @RequiresPermissions("super:all")
    @RequiresAuthentication
    public Result users(Integer currentPage,Integer pageSize){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = userService.page(page, new QueryWrapper<SysUser>().orderByDesc("gmt_create"));
        return Result.success(pageData);
    }

    // 依据账号搜索
    @GetMapping("/search/type")
    @RequiresAuthentication
    public Result searchUsers(Integer currentPage,Integer pageSize,String loginName){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = userService.page(page, new QueryWrapper<SysUser>()
                .orderByDesc("gmt_create")
                .like("login_name", loginName));
        return Result.success(pageData);
    }


    // 登录
    @PostMapping("/login")
    public Result login(@Valid @RequestBody LoginUser loginUser, HttpServletResponse response){
        SysUser user = userService.getOne(new QueryWrapper<SysUser>().eq("login_name", loginUser.getLoginName()));
        Assert.notNull(user,"用户不存在！");
        if (!user.getPassword().equals(SecureUtil.md5(loginUser.getPassword()))){
            return Result.fail("密码错误！");
        }

        //更新登录时间
        userService.update(new UpdateWrapper<SysUser>()
                .eq("user_id",user.getUserId())
                .set("login_date", LocalDateTime.now()));

        //Header返回token
        String jwt = jwtUtils.generateToken(user.getUserId());
        response.setHeader("Authorization",jwt);
        response.setHeader("Access-Control-Expose-Headers", "Authorization");

        //Body返回用户信息
        UserInfo userInfo = new UserInfo();
        BeanUtil.copyProperties(user,userInfo);
        return Result.success(userInfo);
    }

    // 返回头像
    @GetMapping(value = "/avatar/{loginName}",produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] avatar(@PathVariable String loginName){
        SysUser user = userService.getOne(new QueryWrapper<SysUser>().eq("login_name", loginName));

        if (user != null){
            String path = BaseAvatarPath + user.getAvatar();
            File file = new File(path);
            if (file.exists() && file.isFile()){
                try {
                    FileInputStream in = new FileInputStream(file);
                    byte[] bytes = new byte[in.available()];
                    in.read(bytes,0,in.available());
                    in.close();
                    return bytes;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        // throw new RuntimeException("No avatar");
        return null;
    }

    @PostMapping("/add/avatar")
    @RequiresAuthentication
    public Result addAvatar(@RequestParam(value = "avatar") MultipartFile avatar, String loginName){
        Assert.notNull(loginName,"请求账户名不能为空！");
        Assert.notNull(avatar,"请求参数不能为空！");

        SysUser user = userService.getOne(new QueryWrapper<SysUser>().eq("login_name", loginName));
        String avatarPath = null;
        if (user.getAvatar() != null && !user.getAvatar().equals("")){
            avatarPath = BaseAvatarPath + user.getAvatar();
        }else {
            avatarPath = BaseAvatarPath + loginName + ".jpg";
        }

        File userAvatar = new File(avatarPath);
        try {
            avatar.transferTo(userAvatar);
        } catch (IOException e) {
            log.error("上传头像异常:-------------->{}",e.getMessage());
            e.printStackTrace();
        }

        return Result.success("上传成功",null);
    }

    // 添加
    @RequiresAuthentication
    @PostMapping("/add")
    public Result add(@Valid @RequestBody UserForm userForm){
        SysUser user = userService.getOne(new QueryWrapper<SysUser>().eq("login_name", userForm.getLoginName()));
        if (user != null){
            return Result.fail("已存在账号名！");
        }

        //账户信息封装
        SysUser newUser = new SysUser();
        newUser.setLoginName(userForm.getLoginName());
        newUser.setUserName(userForm.getUserName());
        newUser.setPassword(SecureUtil.md5(userForm.getPassword()));
        newUser.setRole(userForm.getRole());
        newUser.setGmtCreate(LocalDateTime.now());
        newUser.setUpdateTime(LocalDateTime.now());
        newUser.setStatus(1);

        userService.save(newUser);
        return Result.success("添加成功！",null);
    }

    // 更新
    @RequiresAuthentication
    @PostMapping("/update")
    public Result update(String loginName
            , String newName
            , String newRole
            , String newStatus){
        System.out.println(loginName);
        SysUser user = userService.getOne(new QueryWrapper<SysUser>().eq("login_name", loginName));
        Assert.notNull(user,"用户不存在！");

        if (newName == null || newRole == null){
            return Result.fail("修改字段不能为空!");
        }

        // 更新信息封装
        SysUser updateUser = new SysUser();
        updateUser.setUserId(user.getUserId());
        updateUser.setUserName(newName);
        updateUser.setRole(newRole);
        updateUser.setStatus(Integer.valueOf(newStatus));
        updateUser.setUpdateTime(LocalDateTime.now());

        userService.updateById(updateUser);
        return Result.success("修改成功！",null);
    }

    // 修改密码
    @RequiresAuthentication
    @PostMapping("/updatePwd")
    public Result updatePwd(String loginName,String oldPwd,String newPwd,String confirmPwd){
        SysUser user = userService.getOne(new QueryWrapper<SysUser>().eq("login_name", loginName));
        Assert.notNull(user,"用户不存在！");
        Assert.notNull(oldPwd,"缺少参数!");
        Assert.notNull(newPwd,"缺少参数！");
        Assert.notNull(confirmPwd,"缺少参数！");


        AccountProfile nowUser = (AccountProfile)SecurityUtils.getSubject().getPrincipal();
        if (!user.getUserId().equals(nowUser.getUserId())){
            return Result.fail("修改账号非当前登录账号！");
        }

        if (!user.getPassword().equals(SecureUtil.md5(oldPwd))){
            return Result.fail("密码错误！");
        }

        if (newPwd.isEmpty() || confirmPwd.isEmpty()){
            return Result.fail("两次密码必须非空！");
        }

        if (!newPwd.equals(confirmPwd)){
            return Result.fail("两次密码不一致！");
        }

        // 更新信息封装
        SysUser updateUser = new SysUser();
        updateUser.setUserId(user.getUserId());
        updateUser.setPassword(SecureUtil.md5(newPwd));
        updateUser.setUpdateTime(LocalDateTime.now());

        userService.updateById(updateUser);
        return Result.success("修改密码成功！",null);
    }

    // 删除
    @RequiresAuthentication
    @PostMapping("/delete")
    public Result delete(String userId){
        SysUser user = userService.getById(Long.parseLong(userId));
        Assert.notNull(user,"账户不存在！");
        if (user.getRole().equals("super")){
            Result.fail("不能删除Root！");
        }

        AccountProfile nowUser = (AccountProfile)SecurityUtils.getSubject().getPrincipal();
        if (nowUser.getUserId().equals(user.getUserId())){
            Result.fail("不能删除当前登录账号！");
        }
        userService.removeById(user.getUserId());
        return Result.success("删除成功！",null);
    }

    // 退出
    @GetMapping("/logout")
    @RequiresAuthentication
    public Result logout() {
        SecurityUtils.getSubject().logout();
        return Result.success(null);
    }
}
