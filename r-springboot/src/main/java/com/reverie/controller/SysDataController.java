package com.reverie.controller;

import cn.hutool.core.map.MapUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.reverie.common.Result;
import com.reverie.entity.SysDaily;
import com.reverie.entity.SysFile;
import com.reverie.entity.SysType;
import com.reverie.mapper.SysDailyMapper;
import com.reverie.service.SysFileService;
import com.reverie.service.SysTypeService;
import com.reverie.util.MyUtils;
import com.reverie.util.RedisUtils;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/data")
public class SysDataController {

    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private SysDailyMapper sysDailyMapper;
    @Autowired
    private SysTypeService typeService;
    @Autowired
    private SysFileService fileService;

    private static final ObjectMapper mapper = new ObjectMapper();

    @GetMapping("/dat-info")
    @ApiOperation(value = "基础数据信息接口", notes = "当日与总downloads-like数据")
    public Result dailyAndTotalInfo(){
        int dailyDownloads = redisUtils.get("dailyDownloads") == null ? 0 : Integer.parseInt(redisUtils.get("dailyDownloads"));
        int dailyLike = redisUtils.get("dailyLike") == null ? 0 : Integer.parseInt(redisUtils.get("dailyLike"));

        List<SysDaily> dailyList = sysDailyMapper.selectList(new QueryWrapper<>());
        int totalDownloads = 0;
        int totalLike = 0;
        for (SysDaily daily : dailyList) {
            totalDownloads += daily.getDailyDownloads();
            totalLike += daily.getDailyLike();
        }

        HashMap<String, Integer> map = new HashMap<>();
        map.put("dailyDownloads",dailyDownloads);
        map.put("dailyLike",dailyLike);
        map.put("totalDownloads",totalDownloads);
        map.put("totalLike",totalLike);

        return Result.success(map);
    }

    @GetMapping("/dal-info")
    @ApiOperation(value = "每日数据信息接口", notes = "每日downloads-like数据")
    public Result downloadsAndLikeTable(){
        List<SysDaily> dailyList = sysDailyMapper.selectList(new QueryWrapper<>());
        List<String> date = new ArrayList<>();
        List<String> downloads = new ArrayList<>();
        List<String> like = new ArrayList<>();
        for (SysDaily daily : dailyList) {
            date.add(MyUtils.dateTransform(daily.getDailyDate()));
            downloads.add(String.valueOf(daily.getDailyDownloads()));
            like.add(String.valueOf(daily.getDailyLike()));
        }
        HashMap<String, List<String>> map = new HashMap<>();
        map.put("date", date);
        map.put("downloads", downloads);
        map.put("like", like);

        return Result.success(map);
    }

    @GetMapping("/top-types")
    @ApiOperation(value = "下载量类型排行", notes = "TOP10下载量最高的类型")
    public Result topTypes(){
        List<SysType> list = typeService.list(new QueryWrapper<SysType>().orderByDesc("downloads").last("limit 10"));
        List<String> types = new ArrayList<>();
        List<String> downloads = new ArrayList<>();
        for (SysType sysType : list) {
            types.add(sysType.getTypeName());
            downloads.add(String.valueOf(sysType.getDownloads()));
        }
        HashMap<String, List<String>> map = new HashMap<>();
        map.put("types",types);
        map.put("downloads",downloads);
        return Result.success(map);
    }

    @GetMapping("/top-files")
    @ApiOperation(value = "下载量文件排行", notes = "TOP10下载量最高的文件")
    public Result topFiles(){
        List<SysFile> list = fileService.list(new QueryWrapper<SysFile>().orderByDesc("downloads").last("limit 10"));
        ArrayList<String> files = new ArrayList<>();
        ArrayList<String> downloads = new ArrayList<>();
        for (SysFile file : list) {
            files.add(file.getFileName());
            downloads.add(String.valueOf(file.getDownloads()));
        }
        HashMap<String, List<String>> map = new HashMap<>();
        map.put("files",files);
        map.put("downloads",downloads);
        return Result.success(map);
    }

    @GetMapping("/notices")
    @ApiOperation(value = "通知", notes = "返回通知信息")
    public Result getNotices(){
        List<String> notices = redisUtils.lRange("notices", 0, -1);
        List<HashMap<String,String>> list = new ArrayList<>();
        for (String notice : notices) {
            try {
                HashMap<String,String> map = mapper.readValue(notice, HashMap.class);
                list.add(map);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return Result.success(list);
    }

    @PostMapping("/notices/add")
    @ApiOperation(value = "添加一个通知")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "notice",value = "类型",dataType = "String",dataTypeClass = String.class),
            @ApiImplicitParam(paramType = "query",name = "info",value = "信息",dataType = "String",dataTypeClass = String.class),})
    public Result pushNotices(String notice ,String info){
        HashMap<String, String> map = new HashMap<>();
        map.put("notice",notice);
        map.put("info",info);
        redisUtils.lRightPush("notices",JSONUtil.toJsonStr(map));
        return Result.success(null);
    }

    @GetMapping("/notices/del")
    @ApiOperation(value = "删除末尾通知")
    public Result popNotices(){
        redisUtils.lRightPop("notices");
        return Result.success(null);
    }
}
