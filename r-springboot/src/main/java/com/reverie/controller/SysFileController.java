package com.reverie.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.reverie.common.Result;
import com.reverie.config.ReverieConfig;
import com.reverie.entity.SysFile;
import com.reverie.entity.SysType;
import com.reverie.service.SysFileService;
import com.reverie.service.SysTypeService;
import com.reverie.util.RedisUtils;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;
import org.springframework.http.MediaType;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Reverie
 * @since 2021-09-14
 */

@Slf4j
@RestController
@RequestMapping("/file")
public class SysFileController {

    @Autowired
    private SysFileService fileService;
    @Autowired
    private SysTypeService typeService;
    @Autowired
    private RedisUtils redisUtils;

    private static final String BaseFilePath = ReverieConfig.getFilePath();
    private static final String BasePicPath = ReverieConfig.getPicPath();

    /**
     * 文件信息列表
     * @param mode 排列方式
     * @return 文件信息
     */
    @GetMapping("/list/{mode}")
    @ApiOperation(value = "依据请求方式获取文件信息列表")
    @ApiImplicitParam(paramType = "path",name = "mode",value = "排列方式 default-默认排列 downloads-文件量排列 like-喜爱量排列",dataType = "String",dataTypeClass = String.class)
    public Result fileList(@PathVariable String mode){
        Assert.notNull(mode,"获取方式不能为空");
        List<SysFile> list = null;
        switch (mode) {
            case "default":
                list = fileService.list(new QueryWrapper<SysFile>().orderByDesc("add_date"));
                break;
            case "downloads":
                list = fileService.list(new QueryWrapper<SysFile>().orderByDesc("downloads"));
                break;
            case "like":
                list = fileService.list(new QueryWrapper<SysFile>().orderByDesc("like"));
                break;
        }
        return Result.success(list);
    }

    @GetMapping("/list/type/{name}")
    @ApiOperation(value = "依据类型名获取文件信息列表")
    @ApiImplicitParam(paramType = "path",name = "name",value = "类型名",dataType = "String",dataTypeClass = String.class)
    public Result fileListByType(@PathVariable String name){
        Assert.notNull(name,"类型名不能为空");
        List<SysFile> list = fileService.list(new QueryWrapper<SysFile>().eq("file_type", name).orderByDesc("add_date"));
        return Result.success(list);
    }

    @GetMapping("/list/search")
    @ApiOperation(value = "依据搜索获取文件信息列表")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "operation",value = "查找依据 file=文件名查找 type=类型名查找",dataType = "String",dataTypeClass = String.class),
            @ApiImplicitParam(paramType = "query",name = "info",value = "搜索关键字",dataType = "String",dataTypeClass = String.class),})
    public Result fileListBySearch(String operation, String info){
        if (operation.equals("file")){
            List<SysFile> list = fileService.list(new QueryWrapper<SysFile>()
                    .orderByDesc("add_date")
                    .like("file_name", info));
            return Result.success(list);
        }else if (operation.equals("type")){
            List<SysFile> list = fileService.list(new QueryWrapper<SysFile>()
                    .orderByDesc("add_date")
                    .like("file_type", info));
            return Result.success(list);
        }
        return Result.fail("搜索失败");
    }

    /**
     * 依据页数搜索
     * @param currentPage 当前页数
     * @param pageSize 页面个数
     * @return 文件信息
     */
    @GetMapping("/search/all")
    @ApiOperation(value = "依据页数搜索文件信息")
    @ApiImplicitParams({@ApiImplicitParam(paramType = "query",name = "currentPage",value = "当前页数",dataType = "String",dataTypeClass = String.class),
                        @ApiImplicitParam(paramType = "query",name = "pageSize",value = "一页数量",dataType = "String",dataTypeClass = String.class)})
    public Result files(Integer currentPage, Integer pageSize){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = fileService.page(page, new QueryWrapper<SysFile>().orderByDesc("add_date"));
        return Result.success(pageData);
    }

    /**
     * 依据类型名搜索
     * @param currentPage 当前页数
     * @param pageSize 页面个数
     * @param operation 搜索类型
     * @param info 信息
     * @return 文件信息
     */
    @GetMapping("/search/operation")
    @ApiIgnore
    public Result typeFiles(Integer currentPage, Integer pageSize, String operation, String info){
        if (operation.equals("file")){
            if(currentPage == null || currentPage < 1) currentPage = 1;
            if(pageSize == null || pageSize < 1) pageSize = 10;
            Page page = new Page(currentPage,pageSize);
            IPage pageData = fileService.page(page, new QueryWrapper<SysFile>()
                    .orderByDesc("add_date")
                    .like("file_name",info));
            return Result.success(pageData);
        }else if (operation.equals("type")){
            if(currentPage == null || currentPage < 1) currentPage = 1;
            if(pageSize == null || pageSize < 1) pageSize = 10;
            Page page = new Page(currentPage,pageSize);
            IPage pageData = fileService.page(page, new QueryWrapper<SysFile>()
                    .orderByDesc("add_date")
                    .eq("file_type",info));
            return Result.success(pageData);
        }
        return Result.fail("搜索失败");
    }

    /**
     * 返回文件图片
     * @param uuid 唯一标识
     * @return 字节流
     */
    @GetMapping(value = "/pic/{uuid}",produces = MediaType.IMAGE_JPEG_VALUE)
    @ApiOperation(value = "返回文件的图片")
    @ApiImplicitParam(paramType = "path",name = "uuid",value = "文件唯一标识uuid",dataType = "String",dataTypeClass = String.class)
    public byte[] filePic(@PathVariable String uuid){
        String path = BasePicPath + uuid + ".jpg";
        File picFile = new File(path);
        try {
            if (picFile.exists()) {
                FileInputStream in = new FileInputStream(picFile);
                byte[] bytes = new byte[in.available()];
                in.read(bytes, 0, in.available());
                in.close();
                return bytes;
            }else {
                String defaultPath = BasePicPath + "default.jpg";
                File defaultFile = new File(defaultPath);
                FileInputStream in = new FileInputStream(defaultFile);
                byte[] bytes = new byte[in.available()];
                in.read(bytes, 0, in.available());
                in.close();
                return bytes;
            }
        } catch (Exception e) {
                e.printStackTrace();
        }

        throw new RuntimeException("No picture");
    }


    /**
     * 下载文件
     * @param uuid UUID
     * @param response 回应
     */
    @GetMapping("/download/{uuid}")
    @ApiOperation(value = "用户下载接口")
    public Result download(@PathVariable String uuid, HttpServletResponse response){
        SysFile ef = fileService.getOne(new QueryWrapper<SysFile>().eq("uuid", uuid));
        if (ef == null){
            return Result.fail("文件信息不存在");
        }

        String filePath = BaseFilePath + ef.getFileType() + "/" + ef.getFileName();
        File file = new File(filePath);
        if (!file.exists()){
            return Result.fail("文件不存在");
        }

        try {
            response.setContentType("application/force-download");// 设置强制下载不打开
            // response.addHeader("Content-Disposition", "attachment;filename=" + new String(ef.getFileName().getBytes("utf-8"),"ISO8859-1"));
            response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(ef.getFileName(),"UTF-8"));
            response.setHeader("Content-Length",String.valueOf(file.length()));

            byte[] buffer = new byte[1024];
            try (FileInputStream fis = new FileInputStream(file); BufferedInputStream bis = new BufferedInputStream(fis)) {
                OutputStream os = response.getOutputStream();
                int i = bis.read(buffer);
                while (i != -1) {
                    os.write(buffer, 0, i);
                    i = bis.read(buffer);
                }
            } catch (Exception e) {
                log.error("传输异常:-------------->{}", ef.getFileName() + e.getMessage());
                e.printStackTrace();
            }
        }catch (Exception e){
            log.error("下载异常:-------------->{}",ef.getFileName()+e.getMessage());
            e.printStackTrace();
        }

        ef.setDownloads(ef.getDownloads()+1);
        fileService.updateById(ef);
        SysType type = typeService.getOne(new QueryWrapper<SysType>().eq("type_name", ef.getFileType()));
        type.setDownloads(type.getDownloads()+1);
        typeService.updateById(type);

        // redis 缓存点击下载+1
        String dailyDownloads = redisUtils.get("dailyDownloads");
        int i = dailyDownloads == null? 1 : Integer.parseInt(dailyDownloads)+1;
        redisUtils.set("dailyDownloads",String.valueOf(i));
        return null;
    }

    /**
     * 点击like
     * @param uuid UUID
     */
    @GetMapping("/like/{uuid}")
    @ApiOperation(value = "点击like+1接口")
    public Result like(@PathVariable String uuid){
        SysFile ef = fileService.getOne(new QueryWrapper<SysFile>().eq("uuid", uuid));
        if (ef == null){
            return Result.fail("文件信息不存在");
        }

        ef.setLikes(ef.getLikes()+1);
        fileService.updateById(ef);

        // redis 缓存点击like+1
        String dailyLike = redisUtils.get("dailyLike");
        int i = dailyLike == null ? 1 : Integer.parseInt(dailyLike)+1;
        redisUtils.set("dailyLike",String.valueOf(i));

        return Result.success("操作成功",null);
    }

    /**
     * 文件信息更新
     * @param fileId 文件Id
     * @param newProvider 新提供者
     * @param newFileName 新文件名
     * @return 操作结果
     */
    @RequiresAuthentication
    @PostMapping("/update")
    @ApiIgnore
    public Result update(String fileId,String newProvider,String newFileName, String newStatus){
        SysFile ef = fileService.getById(fileId);
        if (ef == null){
            return Result.fail("无此文件信息");
        }

        if (!ef.getProvider().equals(newProvider)){
            ef.setProvider(newProvider);
        }
        if(!ef.getStatus().equals(newStatus)){
            ef.setStatus(newStatus);
        }
        if (!ef.getFileName().equals(newFileName)){
            String oldPath = BaseFilePath + ef.getFileType() + "/" + ef.getFileName();
            String newPath = BaseFilePath + ef.getFileType() + "/" + newFileName;
            File file = new File(oldPath);
            if(!file.renameTo(new File(newPath))){
                return Result.fail("文件名修改失败");
            }
            ef.setFileName(newFileName);
        }

        ef.setUpdateDate(LocalDateTime.now());
        fileService.updateById(ef);
        return Result.success("更新成功",null);
    }

    /**
     * 删除文件
     * @param fileId 文件Id
     * @return 操作结果
     */
    @RequiresAuthentication
    @PostMapping("/delete")
    @ApiIgnore
    public Result delete(String fileId){
        SysFile ef = fileService.getById(Integer.parseInt(fileId));
        if (ef == null){
            return Result.fail("无此文件信息");
        }
        String path = BaseFilePath + ef.getFileType() + "/" + ef.getFileName();
        File file = new File(path);
        if (file.exists()){
            file.delete();
        }


        SysType type = typeService.getOne(new QueryWrapper<SysType>().eq("type_name", ef.getFileType()));
        type.setFileNumber(type.getFileNumber()-1);
        type.setDownloads(type.getDownloads()-ef.getDownloads());
        type.setUpdateDate(LocalDateTime.now());

        fileService.removeById(ef.getFileId());
        typeService.updateById(type);
        return Result.success("删除成功",null);
    }
}
