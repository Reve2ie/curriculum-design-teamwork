package com.reverie.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.reverie.common.Result;
import com.reverie.config.ReverieConfig;
import com.reverie.entity.SysFile;
import com.reverie.entity.SysType;
import com.reverie.service.SysFileService;
import com.reverie.service.SysTypeService;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Reverie
 * @since 2021-09-14
 */
@RestController
@RequestMapping("/type")
public class SysTypeController {

    private final static String BaseFilePath = ReverieConfig.getFilePath();;

    @Autowired
    SysTypeService typeService;
    @Autowired
    SysFileService fileService;

    // 所有类型信息
    @GetMapping("/all")
    @RequiresAuthentication
    @ApiIgnore
    public Result all(){
        List<SysType> list = typeService.list(new QueryWrapper<SysType>().orderByDesc("gmt_create"));
        return Result.success(list);
    }

    // 类型信息
    @GetMapping("/search/all")
    @RequiresAuthentication
    @ApiIgnore
    public Result types(Integer currentPage, Integer pageSize){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = typeService.page(page, new QueryWrapper<SysType>().orderByDesc("gmt_create"));
        return Result.success(pageData);
    }

    // 依据类型名关键字搜索
    @GetMapping("/search/type")
    @RequiresAuthentication
    @ApiIgnore
    public Result searchTypes(Integer currentPage, Integer pageSize,String typeName){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = typeService.page(page, new QueryWrapper<SysType>()
                .orderByDesc("gmt_create")
                .like("type_name",typeName));
        return Result.success(pageData);
    }

    // 类型名列表
    @GetMapping("/list")
    @ApiOperation(value = "类型名列表")
    public Result typeList(){
        List<SysType> list = typeService.list(new QueryWrapper<SysType>().orderByDesc("downloads"));
        ArrayList<String> tList = new ArrayList<>();
        for (SysType type : list) {
            tList.add(type.getTypeName());
        }
        return Result.success(tList);
    }

    // 添加类型
    @PostMapping("/add")
    @RequiresAuthentication
    @ApiIgnore
    public Result add(String typeName,String status){
        SysType type = typeService.getOne(new QueryWrapper<SysType>().eq("type_name", typeName));
        if (type != null){
            return Result.fail("已存在该类型！");
        }

        // 类型信息封装
        SysType newType = new SysType();
        newType.setTypeName(typeName);
        newType.setFileNumber(0);
        newType.setGmtCreate(LocalDateTime.now());
        newType.setStatus(status);
        newType.setDownloads(0);

        typeService.save(newType);
        return Result.success("添加成功！",null);
    }

    // 更新类型
    @PostMapping("/update")
    @RequiresAuthentication
    @ApiIgnore
    public Result update(String typeId,String newTypeName,String newCount, String newStatus){
        SysType type = typeService.getById(Integer.parseInt(typeId));
        if (type == null){
            return Result.fail("没有此类型！");
        }

        // 文件信息类型更改
        if (!type.getTypeName().equals(newTypeName)){
            List<SysFile> fileList = fileService.list(new QueryWrapper<SysFile>().eq("file_type", type.getTypeName()));
            if (fileList != null && fileList.size() > 0){
                for (SysFile file : fileList) {
                        file.setFileType(newTypeName);
                        file.setUpdateDate(LocalDateTime.now());
                }
                fileService.updateBatchById(fileList);
            }
            File oldFile = new File(BaseFilePath + type.getTypeName());
            File newFile = new File(BaseFilePath + newTypeName);
            oldFile.renameTo(newFile);
        }

        // 更新信息封装
        SysType newType = new SysType();
        newType.setTypeId(type.getTypeId());
        newType.setTypeName(newTypeName);
        newType.setFileNumber(Integer.parseInt(newCount));
        newType.setStatus(newStatus);
        newType.setUpdateDate(LocalDateTime.now());
        typeService.updateById(newType);

        return Result.success("修改成功！");
    }

    // 删除类型
    @RequiresAuthentication
    @PostMapping("/delete")
    @ApiIgnore
    public Result delete(String typeId){
        SysType type = typeService.getById(Integer.parseInt(typeId));
        if (type == null){
            return Result.fail("没有此类型！");
        }
        int count = fileService.count(new QueryWrapper<SysFile>().eq("file_type", type.getTypeName()));
        if (count != 0){
            return Result.fail("该类型存在文件信息");
        }

        File file = new File(BaseFilePath + type.getTypeName());
        file.delete();
        typeService.removeById(Integer.parseInt(typeId));
        return Result.success("删除成功！");
    }
}
