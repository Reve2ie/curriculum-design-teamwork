package com.reverie.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.reverie.common.Result;
import com.reverie.config.ReverieConfig;
import com.reverie.entity.SysFile;
import com.reverie.entity.SysTemp;
import com.reverie.entity.SysType;
import com.reverie.service.SysFileService;
import com.reverie.service.SysTempService;
import com.reverie.service.SysTypeService;
import com.reverie.util.MyUtils;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Reverie
 * @since 2021-09-14
 */

@Slf4j
@RestController
@RequestMapping("/temp")
public class SysTempController {
    @Autowired
    SysTempService tempService;
    @Autowired
    SysFileService fileService;
    @Autowired
    SysTypeService typeService;

    private final static String BaseTempPath = ReverieConfig.getTempPath();
    private final static String BaseFilePath = ReverieConfig.getFilePath();
    private final static String BasePicPath = ReverieConfig.getPicPath();

    /**
     *
     * @param currentPage 当前页数
     * @param pageSize 页面个数
     * @return 临时文件信息
     */
    @GetMapping("/search/all")
    @RequiresAuthentication
    @ApiIgnore
    public Result files(Integer currentPage, Integer pageSize){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = tempService.page(page, new QueryWrapper<SysTemp>().orderByDesc("gmt_create"));
        return Result.success(pageData);
    }

    /**
     * 依据关键字搜索
     * @param currentPage 当前页数
     * @param pageSize 页面个数
     * @param fileName 关键字
     * @return 临时文件信息
     */
    @GetMapping("/search/type")
    @RequiresAuthentication
    @ApiIgnore
    public Result searchFiles(Integer currentPage, Integer pageSize,String fileName){
        if(currentPage == null || currentPage < 1) currentPage = 1;
        if(pageSize == null || pageSize < 1) pageSize = 10;
        Page page = new Page(currentPage,pageSize);
        IPage pageData = tempService.page(page, new QueryWrapper<SysTemp>()
                .orderByDesc("gmt_create")
                .like("file_name",fileName));
        return Result.success(pageData);
    }

    /** *
     * 上传文件
     * @param files 前端上传文件
     * @param uploader 上传者
     * @param description 描述
     * @return 操作结果
     */
    @PostMapping("/upload")
    @ApiOperation(value = "用户上传接口")
    @ApiImplicitParams({@ApiImplicitParam(paramType = "query",name = "uploader",value = "上传者",dataType = "String",dataTypeClass = String.class),
            @ApiImplicitParam(paramType = "query",name = "description",value = "描述",dataType = "String",dataTypeClass = String.class),
            @ApiImplicitParam(paramType = "body",name = "files",value = "文件",dataType = "MultipartFile",dataTypeClass = MultipartFile.class)})
    public Result upload(@RequestParam(value = "file") MultipartFile[] files, String uploader, String description){
        if (files != null && files.length > 0){
            for (MultipartFile file : files) {
                if (file.isEmpty()){
                    Result.fail("文件为空");
                }
                String fileName = file.getOriginalFilename();
                SysTemp existFile = tempService.getOne(new QueryWrapper<SysTemp>().eq("file_name", fileName));
                if (existFile != null){
                    return Result.fail(fileName+"已存在同名文件");
                }

                // 设置文件存储路径
                String newTempFilePath = BaseTempPath + fileName;
                File newTempFile = new File(newTempFilePath);
                // 判断父目录是否存在
                if (!newTempFile.getParentFile().exists()){
                    newTempFile.setWritable(true,false);
                    newTempFile.getParentFile().mkdirs();
                }
                try {
                    file.transferTo(newTempFile);
                } catch (IOException e) {
                    log.error("上传异常:-------------->{}",fileName+e.getMessage());
                    return Result.fail(fileName+e.getMessage());
                }

                SysTemp temp = new SysTemp();
                temp.setFileName(fileName);
                temp.setFileSize(file.getSize());
                temp.setUploader(uploader);
                temp.setDescription(description);
                temp.setGmtCreate(LocalDateTime.now());
                temp.setUuid(MyUtils.getUuid());
                tempService.save(temp);
            }
            System.out.println(Arrays.toString(files) +uploader+description);
        }
        return Result.success("上传成功",null);
    }

    /**
     * 临时文件转移
     * @param tempId 临时存储Id
     * @param typeName 类型名
     * @return 操作结果
     */
    @RequiresAuthentication
    @PostMapping("/remove")
    @ApiIgnore
    public Result remove(@RequestParam(value = "file") MultipartFile[] files, String tempId,String typeName) throws IOException {
        if (tempId == null || typeName == null){
            return Result.fail("表单信息不能为空");
        }
        SysTemp temp = tempService.getById(Integer.parseInt(tempId));
        if (temp == null){
            return Result.fail("不存在此临时文件");
        }
        SysType type = typeService.getOne(new QueryWrapper<SysType>().eq("type_name", typeName));
        if (type == null){
            return Result.fail("不存在此类型");
        }

        // 文件移动
        String fileName = temp.getFileName();
        String tempFilePath = BaseTempPath + fileName;
        File tempFile = new File(tempFilePath);
        if (!tempFile.exists()){
            Result.fail("文件不存在");
        }

        SysFile ef = fileService.getOne(new QueryWrapper<SysFile>()
                .eq("file_name", temp.getFileName()));
        if (ef != null){
            return Result.fail("已存在此文件");
        }

        // 文件不为空，删除原有文件并在仓库创建新文件
        String filePath = BaseFilePath + typeName + "/" + fileName;
        File file = new File(filePath);
        // 检测是否存在该目录
        if (!file.getParentFile().exists()){
            file.setWritable(true, false);
            file.getParentFile().mkdirs();
        }
        if (!file.exists()){
            file.createNewFile();
        }

        // 输入流 输出流
        try (FileInputStream in = new FileInputStream(tempFile); FileOutputStream out = new FileOutputStream(file, true)) {
            byte[] buffer = new byte[1024];
            while (true) {
                int read = in.read(buffer);
                if (read == -1) break;
                out.write(buffer, 0, read);
            }
        } catch (IOException e) {
            log.error("转移异常:-------------->{}", fileName + e.getMessage());
            return Result.fail(fileName + e.getMessage());
        }

        // 文件信息封装
        SysFile fileToDb = new SysFile();
        fileToDb.setFileName(fileName);
        fileToDb.setFileFormat(fileName.substring(fileName.lastIndexOf(".")+1));
        fileToDb.setFileSize(temp.getFileSize());
        fileToDb.setProvider(temp.getUploader());
        fileToDb.setFileType(typeName);
        fileToDb.setUuid(temp.getUuid());
        fileToDb.setAddDate(LocalDateTime.now());
        fileToDb.setUpdateDate(LocalDateTime.now());
        fileToDb.setDownloads(0);
        fileToDb.setLikes(0);
        fileToDb.setDescription(temp.getDescription());
        fileToDb.setStatus("default");

        // 图片存储
        if (files != null && files.length > 0){
            MultipartFile uploadPicFile = files[0];
            String picPath = BasePicPath + temp.getUuid() + ".jpg";
            File picFile = new File(picPath);
            // 判断父目录是否存在
            if (!picFile.getParentFile().exists()){
                picFile.setWritable(true,false);
                picFile.getParentFile().mkdirs();
            }
            try {
                uploadPicFile.transferTo(picFile);
            } catch (IOException e) {
                log.error("图片上传异常:-------------->{}",picFile.getName()+e.getMessage());
                return Result.fail(picFile.getName()+e.getMessage());
            }
        }
        fileService.save(fileToDb);

        // 更新信息封装
        SysType newType = new SysType();
        newType.setTypeId(type.getTypeId());
        newType.setFileNumber(type.getFileNumber()+1);
        newType.setUpdateDate(LocalDateTime.now());
        typeService.updateById(newType);

        if (tempFile.delete()){
            Result.fail("删除临时文件失败");
        }
        tempService.removeById(tempId);
        return Result.success("移入成功",null);
    }

    @RequiresAuthentication
    @PostMapping("/delete")
    @ApiIgnore
    public Result delete(String tempId){
        SysTemp ef = tempService.getById(tempId);
        if (ef == null){
            return Result.fail("无此文件信息");
        }
        String path = BaseTempPath + ef.getFileName();
        System.out.println(path);
        File file = new File(path);
        if (!file.exists()){
            return Result.fail("无此文件");
        }

        file.delete();
        tempService.removeById(tempId);
        return Result.success("删除成功",null);
    }
}
