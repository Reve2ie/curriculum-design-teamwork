package com.reverie.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

public class MyUtils {
    public static String getUuid(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }

    public static String dateTransform(LocalDate date){
        return DateTimeFormatter.ofPattern("yyyy-MM-dd").format(date);
    }
}
