package com.reverie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.reverie.entity.SysDaily;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Reverie
 * @since 2021-09-14
 */
@Repository
public interface SysDailyMapper extends BaseMapper<SysDaily> {
}
