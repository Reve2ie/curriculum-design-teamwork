package com.reverie.mapper;

import com.reverie.entity.SysFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Reverie
 * @since 2021-09-14
 */
public interface SysFileMapper extends BaseMapper<SysFile> {

}
