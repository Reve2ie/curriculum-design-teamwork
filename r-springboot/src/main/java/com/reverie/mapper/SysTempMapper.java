package com.reverie.mapper;

import com.reverie.entity.SysTemp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Reverie
 * @since 2021-09-14
 */
public interface SysTempMapper extends BaseMapper<SysTemp> {

}
