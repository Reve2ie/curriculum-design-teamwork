package com.reverie.shiro;

import cn.hutool.core.bean.BeanUtil;
import com.reverie.entity.SysUser;
import com.reverie.service.SysUserService;
import com.reverie.util.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class AccountRealm extends AuthorizingRealm {
    @Autowired
    JwtUtils jwtUtils;
    @Autowired
    SysUserService userService;

    // 让realm支持jwt的凭证校验
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JwtToken;
    }

    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        AccountProfile principal = (AccountProfile) principalCollection.getPrimaryPrincipal();

        String pm = principal.getRole();
        if ("super".equals(pm)){
            pm = pm + ":all";
        }

        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        authorizationInfo.addStringPermission(pm);
        return authorizationInfo;
    }

    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        JwtToken jwt = (JwtToken) authenticationToken;
        log.info("jwt----------------->{}", jwt);
        String userId = jwtUtils.getClaimByToken((String) jwt.getPrincipal()).getSubject();
        SysUser user = userService.getById(Long.parseLong(userId));
        if (user == null){
            throw new UnknownAccountException("账户不存在！");
        }
        if (user.getStatus() == -1){
            throw new LockedAccountException("账户已被锁定！");
        }
        AccountProfile profile = new AccountProfile();
        BeanUtil.copyProperties(user,profile);
        log.info("profile----------------->{}", profile.toString());
        return new SimpleAuthenticationInfo(profile,jwt.getCredentials(),getName());
    }
}
