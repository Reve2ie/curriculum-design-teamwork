package com.reverie.shiro;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
public class AccountProfile implements Serializable {
    private Long userId;
    private String loginName;
    private String userName;
    private String role;
}
