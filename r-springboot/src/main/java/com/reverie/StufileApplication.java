package com.reverie;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StufileApplication {

    public static void main(String[] args) {
        SpringApplication.run(StufileApplication.class, args);
    }

}
