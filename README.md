# 小组课程设计

#### 介绍
前后端项目小组实践

#### 软件架构
软件架构说明

+ 前端
  + 后台 Vue+Aioxs
  + 前台 Flutter
+ 后端
  + Springboot
  + Shiro
  + Redis
  + Mysql
  + Mybatis

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
